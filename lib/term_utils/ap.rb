# frozen_string_literal: true

# Copyright (C) 2023 Thomas Baron
#
# This file is part of term_utils.
#
# term_utils is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# term_utils is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with term_utils.  If not, see <https://www.gnu.org/licenses/>.

require 'term_utils/ap/article'
require 'term_utils/ap/flag'
require 'term_utils/ap/parameter'
require 'term_utils/ap/syntax'
require 'term_utils/ap/result'
require 'term_utils/ap/parser'

module TermUtils
  # The Argument Parser module provides a way to parse arguments.
  module AP
    # ParseError.
    class ParseError < StandardError
      # Constructs a new ParseError.
      # @param props [Hash<Symbol, Object>]
      def initialize(props = {})
        super(self.class.create_message(props))
        @props = props.dup
      end

      # Returns the properties associated to this one.
      # @return [Hash<Symbol, Object>]
      def props
        @props.dup
      end

      # Returns the short message (i.e. the message without properties).
      # @return [String] The short message.
      def short_message
        @props.fetch(:message)
      end

      # Tests whether this one has a syntax parameter ID.
      # @return [Boolean]
      def parameter?
        @props.key?(:parameter)
      end

      # Returns the syntax parameter ID (if any).
      # @return [Symbol, nil] The syntax parameter ID if any, `nil` otherwise.
      def parameter
        @props.fetch(:parameter, nil)
      end

      # Tests whether this one has a faulty argument.
      # @return [Boolean]
      def fault?
        @props.key?(:fault)
      end

      # Returns the faulty argument (if any).
      # @return [String, nil] The faulty argument if any, `nil` otherwise.
      def fault
        @props.fetch(:fault, nil)
      end

      # Creates a message based on given properties.
      # @param props [Hash<Symbol, Object>]
      # @return [String]
      def self.create_message(props)
        props = props.dup
        msg = props.delete(:message) { |key| raise StandardError, "#{key} property is mandatory" }
        return msg if props.empty?

        vals = []
        props.each do |key, val|
          vals << "#{key}: \"#{val}\""
        end
        "#{msg} (#{vals.join(', ')})"
      end
    end

    # SyntaxError.
    class SyntaxError < StandardError
    end

    # Creates a new Syntax.
    # @return [TermUtils::AP::Syntax]
    def self.create_syntax(&block)
      new_syntax = TermUtils::AP::Syntax.new
      block.call(new_syntax) if block
      new_syntax
    end

    # Parses a given list of arguments.
    # @param syntax [Syntax]
    # @param arguments [Array<String>]
    # @param opts [Hash]
    # @option opts [Boolean] :strict Whether the Syntax must be considered as strict.
    # @return [Result]
    # @raise [ParseError]
    # @raise [SyntaxError]
    def self.parse_arguments(syntax, arguments, opts = {}, &block)
      TermUtils::AP::Parser.new.parse_arguments(syntax, arguments, opts, &block)
    end
  end
end
