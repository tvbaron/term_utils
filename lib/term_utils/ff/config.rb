# frozen_string_literal: true

# Copyright (C) 2023 Thomas Baron
#
# This file is part of term_utils.
#
# term_utils is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# term_utils is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with term_utils.  If not, see <https://www.gnu.org/licenses/>.

module TermUtils
  module FF
    # Represents a query configuration.
    class Config
      # @return [Array<Regexp>]
      attr_accessor :ignore_list
      # @return [Integer, nil]
      attr_accessor :min_depth
      # @return [Integer, nil]
      attr_accessor :max_depth
      # @return [Symbol, nil] Either `:forward`, `:reverse` or `nil` (default).
      attr_accessor :sorting_mode
      # @return [Proc, nil]
      attr_accessor :sorting_compare

      # Constructs a new Config.
      def initialize
        @ignore_list = []
        @min_depth = nil
        @max_depth = nil
        @sorting_mode = nil
        @sorting_compare = nil
      end

      # Returns whether the search is ordered.
      # @return [Boolean]
      def sorted?
        (@sorting_mode == :forward) || (@sorting_mode == :reverse)
      end

      def initialize_copy(other)
        super
        @ignore_list = @ignore_list.dup
      end
    end
  end
end
