# frozen_string_literal: true

# Copyright (C) 2023 Thomas Baron
#
# This file is part of term_utils.
#
# term_utils is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# term_utils is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with term_utils.  If not, see <https://www.gnu.org/licenses/>.

module TermUtils
  module FF
    # Represents an Entry of a Query result.
    class Entry
      # @return [Integer]
      attr_accessor :index
      # @return [String]
      attr_accessor :name
      # @return [Array<String>]
      attr_accessor :relative_path_comps
      # @return [String]
      attr_accessor :path

      def initialize
        @index = nil
        @name = nil
        @relative_path_comps = nil
        @path = nil
      end

      # @return [Integer]
      def depth
        @relative_path_comps.length
      end
    end
  end
end
