# frozen_string_literal: true

# Copyright (C) 2023 Thomas Baron
#
# This file is part of term_utils.
#
# term_utils is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# term_utils is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with term_utils.  If not, see <https://www.gnu.org/licenses/>.

module TermUtils
  module FF
    # Query search context.
    Context = Struct.new('Context', :config, :base_path, :block, :index_seq, :result)
    # Represents a file system query.
    class Query
      # Constructs a new Query.
      def initialize
        @config = TermUtils::FF::Config.new
      end

      def initialize_copy(other)
        super
        @config = @config.dup
      end

      # Adds a Regexp for ignoring file names.
      # @param regexp [Regexp]
      # @return [Query]
      def ignore(regexp)
        @config.ignore_list << regexp
        self
      end

      # Sets a minimum depth.
      # @param depth [Integer]
      # @return [Query]
      def min_depth(depth)
        @config.min_depth = depth
        self
      end

      # Sets a maximum depth.
      # @param depth [Integer]
      # @return [Query]
      def max_depth(depth)
        @config.max_depth = depth
        self
      end

      # Sets the sorting mode.
      # @param mode [Symbol] Either `:forward`, `:reverse` or `nil` (default).
      # @return [Query]
      def sort(mode = :forward, &block)
        @config.sorting_mode = mode
        @config.sorting_compare = block
        self
      end

      # Executes this one.
      # @param path [String]
      # @return [Array<Entry>]
      def exec(path, &block)
        ctx = Context.new
        ctx.config = @config.dup
        ctx.config.min_depth = 0 if ctx.config.min_depth.nil?
        ctx.base_path = path
        ctx.block = block
        ctx.index_seq = 0
        ctx.result = []
        first_entry = TermUtils::FF::Entry.new
        first_entry.name = path
        first_entry.relative_path_comps = []
        first_entry.path = path
        if ctx.config.min_depth == 0
          first_entry.index = ctx.index_seq
          ctx.index_seq += 1
          ctx.block.call(first_entry) if ctx.block
          ctx.result << first_entry
        end
        if File.directory?(first_entry.path) && (ctx.config.max_depth.nil? || ctx.config.max_depth > 0)
          TermUtils::FF::Query.search(ctx, first_entry)
        end
        ctx.result
      end

      # Searches a directory.
      # @param ctx [Context]
      # @param parent_entry [entry]
      # @return [nil]
      def self.search(ctx, parent_entry)
        entries = Dir.entries(parent_entry.path)
        unless ctx.config.sorting_mode.nil?
          if ctx.config.sorting_compare.nil?
            entries.sort!
          else
            entries.sort!(&ctx.config.sorting_compare)
          end
          if ctx.config.sorting_mode == :reverse
            entries.reverse!
          end
        end
        entries.each do |name|
          next if %w[. ..].include? name
          next unless accept_entry_name?(ctx, name)

          new_entry = TermUtils::FF::Entry.new
          new_entry.name = name
          new_entry.relative_path_comps = parent_entry.relative_path_comps.dup.push(name)
          new_entry.path = "#{ctx.base_path}/#{new_entry.relative_path_comps.join('/')}"
          if ctx.config.min_depth <= new_entry.depth
            new_entry.index = ctx.index_seq
            ctx.index_seq += 1
            ctx.block.call(new_entry) if ctx.block
            ctx.result << new_entry
          end
          if File.directory?(new_entry.path) && (ctx.config.max_depth.nil? || ctx.config.max_depth > new_entry.depth)
            TermUtils::FF::Query.search(ctx, new_entry)
          end
        end
      end

      # Tests whether a given entry should be accepted.
      # @param ctx [Context]
      # @param name [String]
      # @return [Boolean]
      def self.accept_entry_name?(ctx, name)
        ctx.config.ignore_list.each do |e|
          return false if e.match?(name)
        end
        true
      end
    end
  end
end
