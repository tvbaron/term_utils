# frozen_string_literal: true

# Copyright (C) 2023 Thomas Baron
#
# This file is part of term_utils.
#
# term_utils is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# term_utils is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with term_utils.  If not, see <https://www.gnu.org/licenses/>.

module TermUtils
  module AP
    # Represents a Article.
    class Article
      # @return [Symbol]
      attr_accessor :id
      # @return [Integer]
      attr_accessor :min_occurs
      # @return [Integer]
      attr_accessor :max_occurs
      # @return [Symbol] `:integer`, `:string`
      attr_accessor :type
      # @return [String] `%d`, `%s`.
      attr_accessor :format

      # Constructs a new Article.
      # @param opts [Hash]
      # @option opts [Symbol] :id
      # @option opts [Integer] :min_occurs Default value is `1`.
      # @option opts [Integer] :max_occurs Default value is `1`.
      # @option opts [Symbol] :type `:integer`, `:string`.
      # @option opts [Symbol] :format
      def initialize(opts = {})
        @id = opts.fetch(:id, nil)
        @min_occurs = opts.fetch(:min_occurs, 1)
        @max_occurs = opts.fetch(:max_occurs, 1)
        @type = opts.fetch(:type, :string)
        @format = opts.fetch(:format, '%s')
      end

      # Finalizes this one. Internal use.
      # @return [nil]
      def finalize!(opts = {})
        raise TermUtils::AP::SyntaxError, 'min_occurs must be equal or greater than 0' if !@min_occurs.is_a?(Integer) || (@min_occurs < 0)
        raise TermUtils::AP::SyntaxError, 'max_occurs must be equal or greater than min_occurs' if occur_bounded? && (@max_occurs < @min_occurs)

        unless @id
          opts[:anonymous] += 1
          @id = "anonymous#{opts[:anonymous]}".intern
        end
      end

      # Tests whether this one has mutiple occurs.
      # @return [Boolean]
      def multiple_occurs?
        (@max_occurs == nil) || (@max_occurs.is_a?(Integer) && (@max_occurs > 1))
      end

      # Tests whether the number of occurs is fixed.
      # @return [Boolean]
      def occur_bounded?
        (@max_occurs != nil) && @max_occurs.is_a?(Integer)
      end
    end
  end
end
