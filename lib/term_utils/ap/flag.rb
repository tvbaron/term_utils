# frozen_string_literal: true

# Copyright (C) 2023 Thomas Baron
#
# This file is part of term_utils.
#
# term_utils is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# term_utils is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with term_utils.  If not, see <https://www.gnu.org/licenses/>.

module TermUtils
  module AP
    # Represents a Flag.
    class Flag
      # @return [String]
      attr_reader :label
      # @return [Symbol] `:long`, `:short`.
      attr_reader :flavor

      # Constructs a new Flag.
      # @param label [String]
      # @param flavor [Symbol] `:short`, `:long`.
      def initialize(label, flavor)
        raise TermUtils::AP::SyntaxError, 'wrong flag label' if !label.is_a?(String) || /^-+$/.match?(label) || !/^-[0-9A-Za-z_-]+$/.match?(label)
        raise TermUtils::AP::SyntaxError, '' unless %i[short long].include?(flavor)

        @label = label
        @flavor = flavor
      end

      # Tests whether this one is equal to a given Flag.
      def ==(other)
        return false unless other.is_a?(TermUtils::AP::Flag)

        @label == other.label
      end

      # Tests whether this one represents a long flag.
      # @return [Boolean]
      def long?
        @flavor == :long
      end

      # Tests whether this one represents a short flag.
      # @return [Boolean]
      def short?
        @flavor == :short
      end

      # Returns the string representation of this one.
      # @return [String]
      def to_s
        @label
      end
    end
  end
end
