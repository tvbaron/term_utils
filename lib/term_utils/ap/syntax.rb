# frozen_string_literal: true

# Copyright (C) 2023 Thomas Baron
#
# This file is part of term_utils.
#
# term_utils is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# term_utils is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with term_utils.  If not, see <https://www.gnu.org/licenses/>.

module TermUtils
  module AP
    # Represents the argument list syntax. It holds a list of parameters.
    class Syntax
      # @return [Array<Parameter>]
      attr_accessor :parameters

      # Constructs a new Syntax.
      def initialize
        @parameters = []
      end

      # For dup method.
      def initialize_dup(other)
        super(other)
        @parameters = other.parameters.map(&:dup) if other.parameters
      end

      # Finalizes this one. Internal use.
      # @return [nil]
      # @raise [SyntaxError]
      def finalize!(opts = {})
        opts[:anonymous] = 0 unless opts.key? :anonymous
        opts[:flag_labels] = []
        @parameters.each { |p| p.finalize!(opts) }
        nil
      end

      # Creates and adds a new Parameter.
      # @param id [Symbol, nil]
      # @param opts [Hash]
      # @option opts [Symbol] :id
      # @option opts [Integer] :min_occurs
      # @option opts [Integer] :max_occurs
      # @return [Parameter]
      def define_parameter(id = nil, opts = {}, &block)
        if id
          param = @parameters.find { |p| p.id == id }
          if param
            block.call(param) if block
            return param
          end

          opts[:id] = id
        end
        new_parameter = TermUtils::AP::Parameter.new(opts)
        @parameters << new_parameter
        block.call(new_parameter) if block
        new_parameter
      end

      # Fetches all flagged parameters and unflagged parameters.
      # @return [Array]
      def fetch_parameters
        unflagged_params = []
        flagged_params = {}
        shortcut_flags = {}
        @parameters.each do |p|
          if p.flagged?
            # Flagged
            p.flags.each do |f|
              flagged_params[f.label] = p
              if f.long?
                shortcut_flags["#{f.label}="] = f
              else
                shortcut_flags[f.label] = f
              end
            end
          else
            # Unflagged
            unflagged_params << p
          end
        end
        [unflagged_params, flagged_params, shortcut_flags]
      end
    end
  end
end
