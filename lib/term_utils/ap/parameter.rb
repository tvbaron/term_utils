# frozen_string_literal: true

# Copyright (C) 2023 Thomas Baron
#
# This file is part of term_utils.
#
# term_utils is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# term_utils is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with term_utils.  If not, see <https://www.gnu.org/licenses/>.

module TermUtils
  module AP
    # Represents a Parameter.
    class Parameter
      # @return [Symbol]
      attr_accessor :id
      # @return [Integer]
      attr_accessor :min_occurs
      # @return [Integer]
      attr_accessor :max_occurs
      # @return [Array<Flag>]
      attr_accessor :flags
      # @return [Array<Article>]
      attr_accessor :articles

      # Constructs a new Parameter.
      # @param opts [Hash]
      # @option opts [Symbol] :id
      # @option opts [Integer] :min_occurs Default value is `0`.
      # @option opts [Integer] :max_occurs Default value is `1`.
      def initialize(opts = {})
        @id = opts.fetch(:id, nil)
        @min_occurs = opts.fetch(:min_occurs, 0)
        @max_occurs = opts.fetch(:max_occurs, 1)
        @flags = []
        @articles = []
      end

      # For dup method.
      def initialize_dup(other)
        super(other)
        @flags = other.flags.map(&:dup) if other.flags
        @articles = other.articles.map(&:dup) if other.articles
      end

      # Finalizes this one. Internal use.
      # @return [nil]
      # @raise [SyntaxError]
      def finalize!(opts = {})
        raise TermUtils::AP::SyntaxError, 'min_occurs must be equal or greater than 0' if !@min_occurs.is_a?(Integer) || (@min_occurs < 0)
        raise TermUtils::AP::SyntaxError, 'max_occurs must be equal or greater than min_occurs' if occur_bounded? && (@max_occurs < @min_occurs)
        raise TermUtils::AP::SyntaxError, 'empty' if @flags.empty? && @articles.empty?

        unless @id
          opts[:anonymous] += 1
          @id = "anonymous#{opts[:anonymous]}".intern
        end
        @flags.each do |f|
          raise TermUtils::AP::SyntaxError, 'duplicate flag label' if opts[:flag_labels].include?(f.to_s)

          opts[:flag_labels] << f.to_s
        end
        @articles.each { |a| a.finalize!(opts) }
        nil
      end

      # Tests whether this one has mutiple occurs.
      # @return [Boolean]
      def multiple_occurs?
        (@max_occurs == nil) || (@max_occurs.is_a?(Integer) && (@max_occurs > 1))
      end

      # Tests whether the number of occurs is fixed.
      # @return [Boolean]
      def occur_bounded?
        (@max_occurs != nil) && @max_occurs.is_a?(Integer)
      end

      # Tests whether this one is flagged.
      # @return [Boolean]
      def flagged?
        !@flags.empty?
      end

      # Adds a new Flag to this one.
      # @param label [String]
      # @return [Flag]
      def define_flag(label, &block)
        flavor = (label.length == 2) ? :short : :long
        new_flag = TermUtils::AP::Flag.new(label, flavor)
        raise TermUtils::AP::SyntaxError, 'duplicate flag label' if @flags.include? new_flag

        @flags << new_flag
        block.call(new_flag) if block
        new_flag
      end

      # Adds a new Article to this one.
      # @param id [Symbol, nil]
      # @param opts [Hash]
      # @option opts [Symbol] :id
      # @option opts [Integer] :min_occurs Default value is `1`.
      # @option opts [Integer] :max_occurs Default value is `1`.
      # @option opts [Symbol] :type `:integer`, `:string`.
      # @option opts [Symbol] :format
      # @return [Article]
      def define_article(id = nil, opts = {}, &block)
        if id
          art = @articles.find { |p| p.id == id }
          if art
            block.call(art) if block
            return art
          end

          opts[:id] = id
        end
        new_article = TermUtils::AP::Article.new(opts)
        @articles << new_article
        block.call(new_article) if block
        new_article
      end

      # Fetches all flags.
      # @return [Array<Flag>]
      def fetch_flags
        @flags.dup
      end

      # Fetches all articles.
      # @return [Array<Article>]
      def fetch_articles
        @articles.collect(&:dup)
      end
    end
  end
end
