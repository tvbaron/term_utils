# frozen_string_literal: true

# Copyright (C) 2023 Thomas Baron
#
# This file is part of term_utils.
#
# term_utils is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# term_utils is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with term_utils.  If not, see <https://www.gnu.org/licenses/>.

# Utilities for terminal application.
module TermUtils
end

require 'term_utils/ap'
require 'term_utils/ff'
require 'term_utils/tab'
