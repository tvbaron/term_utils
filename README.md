# term_utils

## Purpose

Provides utilities like argument parsing, table formatting and file finding.

## Getting Started

Install term_utils at the command prompt:

    gem install term_utils

Require term_utils in the source file:

    require 'term_utils'

## Content

### Argument Parsing

Exmaple:

    require 'term_utils/ap'
    syntax = TermUtils::AP.create_syntax do |s|
      s.define_parameter :limit do |p|
        p.define_flag '-l'
        p.define_article
      end
      s.define_parameter :path do |p|
        p.max_occurs = nil
        p.define_article
      end
    end
    limit = nil
    paths = []
    TermUtils::AP.parse_arguments(syntax, ARGV) do |on|
      on.parameter :limit do |p|
        limit = p.value
      end
      on.parameter :path do |p|
        paths << p.value
      end
    end
    puts "limit: #{limit}"
    puts "paths:"
    paths.each { |p| puts "  #{p}" }

See the [manual](manual/ARGUMENT_PARSING.md) for more details.

### Table Formatting

Take the following table:

        id  string8   string16
      ----  --------  ----------------
         1  Fiat Lux  Fiat Lux
         2  Alea ...  Alea jacta est
         3  Audac...  Audaces fortuna juvat
      ----  --------  ----------------
        id  string8   string16

You can produce it with the following code:

    require 'term_utils/tab'

    TermUtils::Tab.define_table(:foo) do |t|
      t.define_column :id, :width => 4, :align => :right
      t.define_column :string8, :fixed => true, :ellipsis => "..."
      t.define_column :string16, :width => 16
    end

    data = []
    data << [1, "Fiat Lux", "Fiat Lux"]
    data << [2, "Alea jacta est", "Alea jacta est"]
    data << [3, "Audaces fortuna juvat", "Audaces fortuna juvat"]

    TermUtils::Tab.printer :foo, $stdout, :offset => 2 do |tpr|
      tpr.line
      tpr.header
      tpr.separator
      data.each do |d|
        tpr.data d
      end
      tpr.separator
      tpr.header
      tpr.line
    end

### File Finding

The File Finding module provides a way to query the filesystem.

## Version History

[CHANGELOG](CHANGELOG.md)

## License

GPL-3.0-only.
