require 'term_utils'

TermUtils::Tab.define_table :result do |t|
  t.define_column :index, :align => :right
  t.define_column :depth, :align => :right
  t.define_column :kind, :fixed => true
  t.define_column :name, :width => 24, :fixed => true
  t.define_column :uid, :fixed => true
  t.define_column :gid, :fixed => true
  t.define_column :mode, :fixed => true
  t.define_column :size, :fixed => true
  t.define_column :path, :width => 24
end

entries = TermUtils::FF.find('./lib', './samples') do |q|
  q.use_stat = true
  q.min_depth = 1
end

TermUtils::Tab.printer :result, $stdout, :offset => 2 do |tpr|
  tpr.line
  tpr.header
  tpr.separator
  entries.each do |entry|
    tpr.data [entry.index, entry.depth, entry.kind, entry.name, entry.uid, entry.gid, '%o' % entry.mode, entry.size, entry.path]
  end
  tpr.separator
  tpr.header
  tpr.line
end
