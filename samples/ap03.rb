require 'term_utils/ap'

syntax = TermUtils::AP.create_syntax do |s|
  s.define_parameter :help do |p|
    p.define_flag '--help'
    p.define_flag '-h'
  end
  s.define_parameter :version do |p|
    p.define_flag '--version'
    p.define_flag '-v'
  end
  s.define_parameter :limit do |p|
    p.define_flag '--limit'
    p.define_flag '-l'
    p.define_article
  end
  s.define_parameter :extra do |p|
    p.min_occurs = 2
    p.max_occurs = nil
    p.define_article :e1, max_occurs: 2
    # p.define_article :e2
  end
end

samples = []
samples << %w[foo]

samples.each do |s|
  puts "sample: #{s.join(' ')}"
  begin
    TermUtils::AP.parse_arguments(syntax, s, strict: true) do |on|
      on.parameter do |p|
        puts "  [p] (#{p.param_id}) -> #{p.values.join(', ')}"
      end
      on.article do |a|
        puts "  [a] (#{a.parent.param_id}, #{a.art_id}) -> #{a.value}"
      end
      on.finished do |rem_args|
        puts "  [f] -> #{rem_args.join(', ')}"
      end
    end
  rescue TermUtils::AP::ParseError => e
    puts "short_message: #{e.short_message}"
    puts "parameter: #{e.parameter}" if e.parameter?
    puts "fault: #{e.fault}" if e.fault?
    puts e.message
    puts e.full_message
  end
  puts
end
