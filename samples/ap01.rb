require 'term_utils/ap'

syntax = TermUtils::AP.create_syntax do |s|
  s.define_parameter :pair do |p|
    p.define_flag '--pair'
    p.define_flag '-p'
    p.max_occurs = nil
    p.define_article :key
    p.define_article :value
  end
end

samples = []
samples << %w[-p 0]
samples << %w[-p 0 alfa]
samples << %w[-p 0 alfa bravo]
samples << %w[-p 1 bravo --pair 2 charlie -p 3 delta]

samples.each do |s|
  puts "sample: #{s.join(' ')}"
  begin
    TermUtils::AP.parse_arguments(syntax, s, strict: true) do |on|
      on.parameter do |p|
        puts "  [p] pair: #{p.values.join(', ')}"
      end
      on.article do |a|
        puts "  [a] pair (#{a.art_id}): #{a.value}"
      end
    end
  rescue TermUtils::AP::ParseError => e
    # Handle parse error...
    puts '  error'
  end
  puts
end
