require 'term_utils/tab'

TermUtils::Tab.set_table_defaults :offset => 2
TermUtils::Tab.set_column_defaults :width => 20, :fixed => true, :ellipsis => "..."

my_table = TermUtils::Tab.create_table do |t|
  t.define_column :id, :width => 4, :align => :right
  t.define_column :updated_at do |c|
    c.header.title = "updated at"
    c.format = ->(t) { Time.at(t).utc.strftime("%Y-%m-%d %H:%M:%S") }
  end
  t.define_column :weight, :width => 8, :align => :right, :format => "%.2f"
  t.define_column :path
end

data = []
data << [1, 1573471401, -10.99, "Fiat Lux"]
data << [2, 1573471402,  -2.35, "Alea jacta est"]
data << [3, 1573471403,   4.1,  "Audaces fortuna juvat"]
data << [4, 1573471404,  20.01, "Aquila non capit muscas"]

my_table.printer $stdout do |tpr|
  tpr.line
  tpr.header
  tpr.separator
  data.each do |d|
    tpr.data d
  end
  tpr.separator
  tpr.header
  tpr.line
end
