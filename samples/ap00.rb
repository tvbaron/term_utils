require 'term_utils/ap'

syntax = TermUtils::AP.create_syntax do |s|
  s.define_parameter :help do |p|
    p.define_flag '--help'
    p.define_flag '-h'
  end
  s.define_parameter :version do |p|
    p.define_flag '--version'
    p.define_flag '-v'
  end
  s.define_parameter :limit do |p|
    p.define_flag '--limit'
    p.define_flag '-l'
    p.define_article
  end
  s.define_parameter :path do |p|
    p.max_occurs = nil
    p.define_article
  end
end

samples = []
samples << %w[--help -v]
samples << %w[--version -h]
samples << %w[--limit 1]
samples << %w[-l 2]
samples << %w[alfa bravo]
samples << %w[--limit 3 charlie]
samples << %w[-l 4 delta foxtrot]
samples << %w[golf]

samples.each do |s|
  puts "sample: #{s.join(' ')}"
  TermUtils::AP.parse_arguments(syntax, s) do |on|
    on.parameter :help do
      puts '  help'
    end
    on.parameter :version do
      puts '  version'
    end
    on.parameter :limit do |p|
      puts "  limit: #{p.values.join(', ')}"
    end
    on.parameter :path do |p|
      puts "  path: #{p.values.join(', ')}"
    end
  end
  puts ''
end
