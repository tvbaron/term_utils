require 'term_utils/tab'

TermUtils::Tab.define_table :foo do |t|
  t.define_column :id, :width => 4, :align => :right
  t.define_column :string8, :fixed => true, :ellipsis => "..."
  t.define_column :string16, :width => 16
end

data = []
data << [1, "Fiat Lux", "Fiat Lux"]
data << [2, "Alea jacta est", "Alea jacta est"]
data << [3, "Audaces fortuna juvat", "Audaces fortuna juvat"]

TermUtils::Tab.printer :foo, $stdout, :offset => 2 do |tpr|
  tpr.line
  tpr.header
  tpr.separator
  data.each do |d|
    tpr.data d
  end
  tpr.separator
  tpr.header
  tpr.line
end
