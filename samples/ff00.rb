require 'term_utils/ff'

query = TermUtils::FF::Query.new
query.ignore /^[.]git$/
query.ignore /^[.]yardoc$/
query.min_depth 1
query.max_depth 3
query.sort
query.exec('.') do |entry|
  puts entry.path
end
