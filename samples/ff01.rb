require 'term_utils'

TermUtils::Tab.define_table :result do |t|
  t.define_column :index, :align => :right
  t.define_column :depth, :align => :right
  t.define_column :name, :width => 24, :fixed => true
  t.define_column :path, :width => 24
end

query = TermUtils::FF::Query.new
query.ignore /^[.]git$/
query.ignore /^[.]yardoc$/
query.min_depth 1
query.sort

TermUtils::Tab.printer :result, $stdout, :offset => 2 do |tpr|
  tpr.line
  tpr.header
  tpr.separator
  query.exec('.') do |entry|
    tpr.data [entry.index, entry.depth, entry.name, entry.path]
  end
  tpr.separator
  tpr.header
  tpr.line
end
