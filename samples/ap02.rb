require 'term_utils/ap'

syntax = TermUtils::AP.create_syntax do |s|
  s.define_parameter :pair do |p|
    p.define_article :key
    p.define_article :value, max_occurs: nil
  end
end

samples = []
samples << %w[alfa bravo charlie]

samples.each do |s|
  puts "sample: #{s.join(' ')}"
  TermUtils::AP.parse_arguments(syntax, s) do |on|
    on.parameter do |p|
      puts "  [p] (#{p.param_id}): #{p.values.join(', ')}"
    end
    on.article do |a|
      puts "  [a] (#{a.parent.param_id}) (#{a.art_id}): #{a.value}"
    end
    on.article :pair, :key do |a|
      puts "  [a] pair key: #{a.value}"
    end
    on.article :pair, :value do |a|
      puts "  [a] pair value: #{a.value}"
    end
  end
  puts
end
