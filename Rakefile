# Rakefile for term_utils

require 'rubygems'

GEM_SPEC = Gem::Specification::load("term_utils.gemspec")
GEM_NAME = GEM_SPEC.name
GEM_VERSION = GEM_SPEC.version

GEM = "#{GEM_NAME}-#{GEM_VERSION}.gem"

task :default => :help

desc "Print help."
task :help do
  puts <<-EOS
usage: rake <target>...

Available targets:
  doc           Build doc.
  gem           Build gem.
  clean         Remove doc and gem.
  install       Install gem.
  uninstall     Uninstall gem.

To deploy a gem:
  rm .doc
  rake doc
  rake gem
  gem push #{GEM}
EOS
end

desc "Create gem."
task :gem => GEM

desc "Generate documentation."
task :doc => %w[.doc]

file ".doc" do
  sh "yardoc"
  sh "touch .doc"
end

file GEM => %w[.doc] do
  sh "gem build #{GEM_NAME}"
end

desc "Install gem."
task :install => :gem do
  sh "gem install #{GEM}"
end

desc "Uninstall gem."
task :uninstall do
  sh "gem uninstall #{GEM_NAME} --version #{GEM_VERSION}"
end

desc "Remove documentation and gem."
task :clean do
  sh "rm -rf .doc doc #{GEM}"
end
