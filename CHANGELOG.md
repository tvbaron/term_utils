# term_utils - Changelog

### 0.5.0 - 2023-08-09

### Added

- Added AP::ParseError properties: parameter and fault.
- Added FF.find.

### 0.4.0 - 2020-08-02

### Changed

- Changed AP API.
- Changed FF API.

## 0.3.2 - 2020-02-07

### Changed

- Moved repository to gitlab.

## 0.3.1 - 2019-11-16

### Added

- Added Tab::Table#set_table_defaults to set table default properties.
- Added Tab::Table#set_column_defaults to set column default properties.
- Added column header (Tab).
- Added AP::Result#collect.

### Fixed

- Fixed ellipsis greater than column width.

## 0.3.0 - 2019-11-10

### Added

- Added Argument Parsing module.

## 0.2.0 - 2019-10-17

### Added

- Added File Finder module.

## 0.1.1 - 2019-10-16

### Added

- Added documentation and samples for tab module.

## 0.1.0 - 2019-10-15

### Added

- Initial base code.
