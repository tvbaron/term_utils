require 'term_utils/property_tree_node'
include TermUtils
RSpec.describe PropertyTreeNode do
  before(:all) do
    # a0:
    # - b0:
    #   - c0
    # - b1:
    #   - d0
    #   - d1
    # - b2:
    #   - e0
    #   - e1
    #   - e2
    @na0 = PropertyTreeNode.new :key => :a0, :value => "alfa"
    @nb0 = @na0.define_node :key => :b0, :value => "bravo"
    @nc0 = @nb0.define_node :key => :c0, :value => "charlie"
    @nb1 = @na0.define_node :key => :b1, :value => "delta"
    @nd0 = @nb1.define_node :key => :d0, :value => "echo"
    @nd1 = @nb1.define_node :key => :d1, :value => "foxtrot"
    @nb2 = @na0.define_node :key => :b2, :value => "golf"
    @ne0 = @nb2.define_node :key => :e0, :value => "hotel"
    @ne1 = @nb2.define_node :key => :e1, :value => "india"
    @ne2 = @nb2.define_node :key => :e2, :value => "juliet"
  end
  it "tests head?" do
    expect(@na0.head?).to be(true)
    expect(@nb0.head?).to be(false)
    expect(@nc0.head?).to be(false)
  end
  it "tests leaf?" do
    expect(@na0.leaf?).to be(false)
    expect(@nb0.leaf?).to be(false)
    expect(@nc0.leaf?).to be(true)
  end
  it "tests child_node" do
    expect(@na0.child_node(:b0)).to be(@nb0)
    expect(@na0.child_node(:b1)).to be(@nb1)
    expect(@na0.child_node(:b2)).to be(@nb2)
    expect(@na0.child_node(:b3)).to be(nil)
  end
  it "tests path" do
    expect(@na0.path).to eq([:a0])
    expect(@nb0.path).to eq([:a0, :b0])
    expect(@nc0.path).to eq([:a0, :b0, :c0])
    expect(@nb1.path).to eq([:a0, :b1])
    expect(@nd0.path).to eq([:a0, :b1, :d0])
    expect(@nd1.path).to eq([:a0, :b1, :d1])
    expect(@nb2.path).to eq([:a0, :b2])
    expect(@ne0.path).to eq([:a0, :b2, :e0])
    expect(@ne1.path).to eq([:a0, :b2, :e1])
    expect(@ne2.path).to eq([:a0, :b2, :e2])
  end
  it "tests each_node" do
    nodes = []
    @na0.each_node { |n| nodes << n }
    expect(nodes.length).to eq(10)
    expect(nodes.shift).to eq(@na0)
    expect(nodes.shift).to eq(@nb0)
    expect(nodes.shift).to eq(@nc0)
    expect(nodes.shift).to eq(@nb1)
    expect(nodes.shift).to eq(@nd0)
    expect(nodes.shift).to eq(@nd1)
    expect(nodes.shift).to eq(@nb2)
    expect(nodes.shift).to eq(@ne0)
    expect(nodes.shift).to eq(@ne1)
    expect(nodes.shift).to eq(@ne2)
  end
  it "tests each_node with path" do
    nodes = []
    @na0.each_node(:path => [:a0, :b2]) { |n| nodes << n }
    expect(nodes.length).to eq(4)
    expect(nodes.shift).to eq(@nb2)
    expect(nodes.shift).to eq(@ne0)
    expect(nodes.shift).to eq(@ne1)
    expect(nodes.shift).to eq(@ne2)
  end
  it "tests each_node with leaf_only" do
    nodes = []
    @na0.each_node(:leaf_only => true) { |n| nodes << n }
    expect(nodes.length).to eq(6)
    expect(nodes.shift).to eq(@nc0)
    expect(nodes.shift).to eq(@nd0)
    expect(nodes.shift).to eq(@nd1)
    expect(nodes.shift).to eq(@ne0)
    expect(nodes.shift).to eq(@ne1)
    expect(nodes.shift).to eq(@ne2)
  end
  it "tests each_node with path and leaf_only" do
    nodes = []
    @na0.each_node(:path => [:a0, :b1], :leaf_only => true) { |n| nodes << n }
    expect(nodes.length).to eq(2)
    expect(nodes.shift).to eq(@nd0)
    expect(nodes.shift).to eq(@nd1)
  end
  it "tests collect_nodes" do
    nodes = @na0.collect_nodes
    expect(nodes.length).to eq(10)
    expect(nodes.shift).to eq(@na0)
    expect(nodes.shift).to eq(@nb0)
    expect(nodes.shift).to eq(@nc0)
    expect(nodes.shift).to eq(@nb1)
    expect(nodes.shift).to eq(@nd0)
    expect(nodes.shift).to eq(@nd1)
    expect(nodes.shift).to eq(@nb2)
    expect(nodes.shift).to eq(@ne0)
    expect(nodes.shift).to eq(@ne1)
    expect(nodes.shift).to eq(@ne2)
  end
  it "tests collect_paths" do
    paths = @na0.collect_paths
    expect(paths.length).to eq(10)
    expect(paths.shift).to eq([:a0])
    expect(paths.shift).to eq([:a0, :b0])
    expect(paths.shift).to eq([:a0, :b0, :c0])
    expect(paths.shift).to eq([:a0, :b1])
    expect(paths.shift).to eq([:a0, :b1, :d0])
    expect(paths.shift).to eq([:a0, :b1, :d1])
    expect(paths.shift).to eq([:a0, :b2])
    expect(paths.shift).to eq([:a0, :b2, :e0])
    expect(paths.shift).to eq([:a0, :b2, :e1])
    expect(paths.shift).to eq([:a0, :b2, :e2])
  end
  it "tests collect_values" do
    vals = @na0.collect_values
    expect(vals.length).to eq(10)
    expect(vals.shift).to eq("alfa")
    expect(vals.shift).to eq("bravo")
    expect(vals.shift).to eq("charlie")
    expect(vals.shift).to eq("delta")
    expect(vals.shift).to eq("echo")
    expect(vals.shift).to eq("foxtrot")
    expect(vals.shift).to eq("golf")
    expect(vals.shift).to eq("hotel")
    expect(vals.shift).to eq("india")
    expect(vals.shift).to eq("juliet")
  end
  it "tests find_node" do
    expect(@na0.find_node([:a0])).to eq(@na0)
    expect(@na0.find_node([:a0, :b0])).to eq(@nb0)
    expect(@na0.find_node([:a0, :b0, :c0])).to eq(@nc0)
    expect(@na0.find_node([:a0, :b0, :c1])).to eq(nil)
    expect(@na0.find_node([:a0, :b1])).to eq(@nb1)
    expect(@na0.find_node([:a0, :b1, :d0])).to eq(@nd0)
    expect(@na0.find_node([:a0, :b1, :d1])).to eq(@nd1)
    expect(@na0.find_node([:a0, :b1, :d2])).to eq(nil)
    expect(@na0.find_node([:a0, :b2])).to eq(@nb2)
    expect(@na0.find_node([:a0, :b2, :e0])).to eq(@ne0)
    expect(@na0.find_node([:a0, :b2, :e1])).to eq(@ne1)
    expect(@na0.find_node([:a0, :b2, :e2])).to eq(@ne2)
    expect(@na0.find_node([:a0, :b2, :e3])).to eq(nil)
    expect(@na0.find_node([:a0, :b3])).to eq(nil)
    expect(@na0.find_node([:a1])).to eq(nil)
  end
  it "tests node_exists?" do
    expect(@na0.node_exists?([:a0])).to eq(true)
    expect(@na0.node_exists?([:a0, :b0])).to eq(true)
    expect(@na0.node_exists?([:a0, :b0, :c0])).to eq(true)
    expect(@na0.node_exists?([:a0, :b0, :c1])).to eq(false)
    expect(@na0.node_exists?([:a0, :b1])).to eq(true)
    expect(@na0.node_exists?([:a0, :b1, :d0])).to eq(true)
    expect(@na0.node_exists?([:a0, :b1, :d1])).to eq(true)
    expect(@na0.node_exists?([:a0, :b1, :d2])).to eq(false)
    expect(@na0.node_exists?([:a0, :b2])).to eq(true)
    expect(@na0.node_exists?([:a0, :b2, :e0])).to eq(true)
    expect(@na0.node_exists?([:a0, :b2, :e1])).to eq(true)
    expect(@na0.node_exists?([:a0, :b2, :e2])).to eq(true)
    expect(@na0.node_exists?([:a0, :b2, :e3])).to eq(false)
    expect(@na0.node_exists?([:a0, :b3])).to eq(false)
    expect(@na0.node_exists?([:a1])).to eq(false)
  end
  it "tests eval_child_count" do
    expect(@na0.eval_child_count([:a0])).to eq(3)
    expect(@na0.eval_child_count([:a0, :b0])).to eq(1)
    expect(@na0.eval_child_count([:a0, :b0, :c0])).to eq(0)
    expect(@na0.eval_child_count([:a0, :b0, :c1])).to eq(nil)
    expect(@na0.eval_child_count([:a0, :b1])).to eq(2)
    expect(@na0.eval_child_count([:a0, :b1, :d0])).to eq(0)
    expect(@na0.eval_child_count([:a0, :b1, :d1])).to eq(0)
    expect(@na0.eval_child_count([:a0, :b1, :d2])).to eq(nil)
    expect(@na0.eval_child_count([:a0, :b2])).to eq(3)
    expect(@na0.eval_child_count([:a0, :b2, :e0])).to eq(0)
    expect(@na0.eval_child_count([:a0, :b2, :e1])).to eq(0)
    expect(@na0.eval_child_count([:a0, :b2, :e2])).to eq(0)
    expect(@na0.eval_child_count([:a0, :b2, :e3])).to eq(nil)
    expect(@na0.eval_child_count([:a0, :b3])).to eq(nil)
    expect(@na0.eval_child_count([:a1])).to eq(nil)
  end
  it "tests find_node_value" do
    expect(@na0.find_node_value([:a0])).to eq("alfa")
    expect(@na0.find_node_value([:a0, :b0])).to eq("bravo")
    expect(@na0.find_node_value([:a0, :b0, :c0])).to eq("charlie")
    expect(@na0.find_node_value([:a0, :b0, :c1])).to eq(nil)
    expect(@na0.find_node_value([:a0, :b1])).to eq("delta")
    expect(@na0.find_node_value([:a0, :b1, :d0])).to eq("echo")
    expect(@na0.find_node_value([:a0, :b1, :d1])).to eq("foxtrot")
    expect(@na0.find_node_value([:a0, :b1, :d2])).to eq(nil)
    expect(@na0.find_node_value([:a0, :b2])).to eq("golf")
    expect(@na0.find_node_value([:a0, :b2, :e0])).to eq("hotel")
    expect(@na0.find_node_value([:a0, :b2, :e1])).to eq("india")
    expect(@na0.find_node_value([:a0, :b2, :e2])).to eq("juliet")
    expect(@na0.find_node_value([:a0, :b2, :e3])).to eq(nil)
    expect(@na0.find_node_value([:a0, :b3])).to eq(nil)
    expect(@na0.find_node_value([:a1])).to eq(nil)
  end
  after(:all) do
  end
end
