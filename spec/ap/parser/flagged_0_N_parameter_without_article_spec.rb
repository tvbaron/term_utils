require 'term_utils/ap'
include TermUtils
RSpec.describe AP::Parser do
  describe "flagged 0-N parameter without article" do
    before(:all) do
      @syntax = AP.create_syntax do |s|
        s.define_parameter :verbose do |p|
          p.define_flag '--verbose'
          p.define_flag '-v'
          p.max_occurs = nil
        end
      end
    end
    it "tests success (0)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[])
      value_params = res.find_parameters(:value)
      expect(value_params.length).to eq(0)
    end
    it "tests success (1)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[--verbose])
      verbose_params = res.find_parameters(:verbose)
      expect(verbose_params.length).to eq(1)
    end
    it "tests success (2)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[-v])
      verbose_params = res.find_parameters(:verbose)
      expect(verbose_params.length).to eq(1)
    end
    it "tests success (3)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[--verbose --verbose])
      verbose_params = res.find_parameters(:verbose)
      expect(verbose_params.length).to eq(2)
    end
    it "tests success (4)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[-v -v])
      verbose_params = res.find_parameters(:verbose)
      expect(verbose_params.length).to eq(2)
    end
    it "tests success (5)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[--verbose --verbose --verbose])
      verbose_params = res.find_parameters(:verbose)
      expect(verbose_params.length).to eq(3)
    end
    it "tests success (6)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[-v -v -v])
      verbose_params = res.find_parameters(:verbose)
      expect(verbose_params.length).to eq(3)
    end
  end
end
