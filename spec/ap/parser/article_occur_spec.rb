require 'term_utils/ap'
include TermUtils

RSpec.describe AP::Parser do
  describe 'article occur' do
    describe 'flagged parameter 0-2 articles' do
      before(:all) do
        @syntax = AP.create_syntax do |s|
          s.define_parameter :limit do |p|
            p.define_flag '-l'
            p.define_article do |a|
              a.min_occurs = 0
              a.max_occurs = 2
            end
          end
        end
      end
      it 'tests success (1)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[-l])
        expect(res.results.length).to eq(1)
        expect(res.results[0].param_id).to eq(:limit)
        expect(res.results[0].results.length).to eq(0)
        expect(res.remaining_arguments.length).to eq(0)
      end
      it 'tests success (2)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[-l 1])
        expect(res.results.length).to eq(1)
        expect(res.results[0].param_id).to eq(:limit)
        expect(res.results[0].results.length).to eq(1)
        expect(res.results[0].results[0].value).to eq('1')
        expect(res.remaining_arguments.length).to eq(0)
      end
      it 'tests success (3)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[-l 1 2])
        expect(res.results.length).to eq(1)
        expect(res.results[0].param_id).to eq(:limit)
        expect(res.results[0].results.length).to eq(2)
        expect(res.results[0].results[0].value).to eq('1')
        expect(res.results[0].results[1].value).to eq('2')
        expect(res.remaining_arguments.length).to eq(0)
      end
      it 'tests success (4)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[-l 1 2 3])
        expect(res.results.length).to eq(1)
        expect(res.results[0].param_id).to eq(:limit)
        expect(res.results[0].results.length).to eq(2)
        expect(res.results[0].results[0].value).to eq('1')
        expect(res.results[0].results[1].value).to eq('2')
        expect(res.remaining_arguments.length).to eq(1)
        expect(res.remaining_arguments[0]).to eq('3')
      end
      it 'tests failure (1)' do
        expect do
          AP::Parser.new.parse_arguments(@syntax, %w[-l 1 2 3], strict: true)
        end .to raise_error AP::ParseError
      end
    end
    describe 'flagged parameter 1-3 articles' do
      before(:all) do
        @syntax = AP.create_syntax do |s|
          s.define_parameter :limit do |p|
            p.define_flag '-l'
            p.define_article do |a|
              a.min_occurs = 1
              a.max_occurs = 3
            end
          end
        end
      end
      it 'tests failure (1)' do
        expect do
          AP::Parser.new.parse_arguments(@syntax, %w[-l])
        end .to raise_error AP::ParseError
      end
      it 'tests success (1)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[-l 1])
        expect(res.results.length).to eq(1)
        expect(res.results[0].param_id).to eq(:limit)
        expect(res.results[0].results.length).to eq(1)
        expect(res.results[0].results[0].value).to eq('1')
        expect(res.remaining_arguments.length).to eq(0)
      end
      it 'tests success (2)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[-l 1 2])
        expect(res.results.length).to eq(1)
        expect(res.results[0].param_id).to eq(:limit)
        expect(res.results[0].results.length).to eq(2)
        expect(res.results[0].results[0].value).to eq('1')
        expect(res.results[0].results[1].value).to eq('2')
        expect(res.remaining_arguments.length).to eq(0)
      end
      it 'tests success (3)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[-l 1 2 3])
        expect(res.results.length).to eq(1)
        expect(res.results[0].param_id).to eq(:limit)
        expect(res.results[0].results.length).to eq(3)
        expect(res.results[0].results[0].value).to eq('1')
        expect(res.results[0].results[1].value).to eq('2')
        expect(res.results[0].results[2].value).to eq('3')
        expect(res.remaining_arguments.length).to eq(0)
      end
      it 'tests success (4)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[-l 1 2 3 4])
        expect(res.results.length).to eq(1)
        expect(res.results[0].param_id).to eq(:limit)
        expect(res.results[0].results.length).to eq(3)
        expect(res.results[0].results[0].value).to eq('1')
        expect(res.results[0].results[1].value).to eq('2')
        expect(res.results[0].results[2].value).to eq('3')
        expect(res.remaining_arguments.length).to eq(1)
        expect(res.remaining_arguments[0]).to eq('4')
      end
      it 'tests failure (2)' do
        expect do
          AP::Parser.new.parse_arguments(@syntax, %w[-l 1 2 3 4], strict: true)
        end .to raise_error AP::ParseError
      end
    end
    describe 'flagged parameter 2-4 articles' do
      before(:all) do
        @syntax = AP.create_syntax do |s|
          s.define_parameter :limit do |p|
            p.define_flag '-l'
            p.define_article do |a|
              a.min_occurs = 2
              a.max_occurs = 4
            end
          end
        end
      end
      it 'tests failure (1)' do
        expect do
          AP::Parser.new.parse_arguments(@syntax, %w[-l])
        end .to raise_error AP::ParseError
      end
      it 'tests failure (2)' do
        expect do
          AP::Parser.new.parse_arguments(@syntax, %w[-l 1])
        end .to raise_error AP::ParseError
      end
      it 'tests success (1)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[-l 1 2])
        expect(res.results.length).to eq(1)
        expect(res.results[0].param_id).to eq(:limit)
        expect(res.results[0].results.length).to eq(2)
        expect(res.results[0].results[0].value).to eq('1')
        expect(res.results[0].results[1].value).to eq('2')
        expect(res.remaining_arguments.length).to eq(0)
      end
      it 'tests success (2)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[-l 1 2 3])
        expect(res.results.length).to eq(1)
        expect(res.results[0].param_id).to eq(:limit)
        expect(res.results[0].results.length).to eq(3)
        expect(res.results[0].results[0].value).to eq('1')
        expect(res.results[0].results[1].value).to eq('2')
        expect(res.results[0].results[2].value).to eq('3')
        expect(res.remaining_arguments.length).to eq(0)
      end
      it 'tests success (3)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[-l 1 2 3 4])
        expect(res.results.length).to eq(1)
        expect(res.results[0].param_id).to eq(:limit)
        expect(res.results[0].results.length).to eq(4)
        expect(res.results[0].results[0].value).to eq('1')
        expect(res.results[0].results[1].value).to eq('2')
        expect(res.results[0].results[2].value).to eq('3')
        expect(res.results[0].results[3].value).to eq('4')
        expect(res.remaining_arguments.length).to eq(0)
      end
      it 'tests success (4)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[-l 1 2 3 4 5])
        expect(res.results.length).to eq(1)
        expect(res.results[0].param_id).to eq(:limit)
        expect(res.results[0].results.length).to eq(4)
        expect(res.results[0].results[0].value).to eq('1')
        expect(res.results[0].results[1].value).to eq('2')
        expect(res.results[0].results[2].value).to eq('3')
        expect(res.results[0].results[3].value).to eq('4')
        expect(res.remaining_arguments.length).to eq(1)
        expect(res.remaining_arguments[0]).to eq('5')
      end
      it 'tests failure (2)' do
        expect do
          AP::Parser.new.parse_arguments(@syntax, %w[-l 1 2 3 4 5], strict: true)
        end .to raise_error AP::ParseError
      end
    end
    describe 'unflagged parameter 0-2 articles' do
      before(:all) do
        @syntax = AP.create_syntax do |s|
          s.define_parameter :path do |p|
            p.define_article do |a|
              a.min_occurs = 0
              a.max_occurs = 2
            end
          end
        end
      end
      it 'tests success (1)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[])
        expect(res.results.length).to eq(0)
        expect(res.remaining_arguments.length).to eq(0)
      end
      it 'tests success (2)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[alfa])
        expect(res.results.length).to eq(1)
        expect(res.results[0].param_id).to eq(:path)
        expect(res.results[0].results.length).to eq(1)
        expect(res.results[0].results[0].value).to eq('alfa')
        expect(res.remaining_arguments.length).to eq(0)
      end
      it 'tests success (3)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[alfa bravo])
        expect(res.results.length).to eq(1)
        expect(res.results[0].param_id).to eq(:path)
        expect(res.results[0].results.length).to eq(2)
        expect(res.results[0].results[0].value).to eq('alfa')
        expect(res.results[0].results[1].value).to eq('bravo')
        expect(res.remaining_arguments.length).to eq(0)
      end
      it 'tests success (4)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[alfa bravo charlie])
        expect(res.results.length).to eq(1)
        expect(res.results[0].param_id).to eq(:path)
        expect(res.results[0].results.length).to eq(2)
        expect(res.results[0].results[0].value).to eq('alfa')
        expect(res.results[0].results[1].value).to eq('bravo')
        expect(res.remaining_arguments.length).to eq(1)
        expect(res.remaining_arguments[0]).to eq('charlie')
      end
      it 'tests failure (1)' do
        expect do
          AP::Parser.new.parse_arguments(@syntax, %w[alfa bravo charlie], strict: true)
        end .to raise_error AP::ParseError
      end
    end
    describe 'unflagged parameter 1-3 articles' do
      before(:all) do
        @syntax = AP.create_syntax do |s|
          s.define_parameter :path do |p|
            p.define_article do |a|
              a.min_occurs = 1
              a.max_occurs = 3
            end
          end
        end
      end
      it 'tests success (1)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[alfa])
        expect(res.results.length).to eq(1)
        expect(res.results[0].param_id).to eq(:path)
        expect(res.results[0].results.length).to eq(1)
        expect(res.results[0].results[0].value).to eq('alfa')
        expect(res.remaining_arguments.length).to eq(0)
      end
      it 'tests success (2)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[alfa bravo])
        expect(res.results.length).to eq(1)
        expect(res.results[0].param_id).to eq(:path)
        expect(res.results[0].results.length).to eq(2)
        expect(res.results[0].results[0].value).to eq('alfa')
        expect(res.results[0].results[1].value).to eq('bravo')
        expect(res.remaining_arguments.length).to eq(0)
      end
      it 'tests success (3)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[alfa bravo charlie])
        expect(res.results.length).to eq(1)
        expect(res.results[0].param_id).to eq(:path)
        expect(res.results[0].results.length).to eq(3)
        expect(res.results[0].results[0].value).to eq('alfa')
        expect(res.results[0].results[1].value).to eq('bravo')
        expect(res.results[0].results[2].value).to eq('charlie')
        expect(res.remaining_arguments.length).to eq(0)
      end
      it 'tests success (4)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[alfa bravo charlie delta])
        expect(res.results.length).to eq(1)
        expect(res.results[0].param_id).to eq(:path)
        expect(res.results[0].results.length).to eq(3)
        expect(res.results[0].results[0].value).to eq('alfa')
        expect(res.results[0].results[1].value).to eq('bravo')
        expect(res.results[0].results[2].value).to eq('charlie')
        expect(res.remaining_arguments.length).to eq(1)
        expect(res.remaining_arguments[0]).to eq('delta')
      end
      it 'tests failure (1)' do
        expect do
          AP::Parser.new.parse_arguments(@syntax, %w[alfa bravo charlie delta], strict: true)
        end .to raise_error AP::ParseError
      end
    end
    describe 'flagged parameter 2-4 articles' do
      before(:all) do
        @syntax = AP.create_syntax do |s|
          s.define_parameter :path do |p|
            p.define_article do |a|
              a.min_occurs = 2
              a.max_occurs = 4
            end
          end
        end
      end
      it 'tests failure (1)' do
        expect do
          AP::Parser.new.parse_arguments(@syntax, %w[alfa])
        end .to raise_error AP::ParseError
      end
      it 'tests success (1)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[alfa bravo])
        expect(res.results.length).to eq(1)
        expect(res.results[0].param_id).to eq(:path)
        expect(res.results[0].results.length).to eq(2)
        expect(res.results[0].results[0].value).to eq('alfa')
        expect(res.results[0].results[1].value).to eq('bravo')
        expect(res.remaining_arguments.length).to eq(0)
      end
      it 'tests success (2)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[alfa bravo charlie])
        expect(res.results.length).to eq(1)
        expect(res.results[0].param_id).to eq(:path)
        expect(res.results[0].results.length).to eq(3)
        expect(res.results[0].results[0].value).to eq('alfa')
        expect(res.results[0].results[1].value).to eq('bravo')
        expect(res.results[0].results[2].value).to eq('charlie')
        expect(res.remaining_arguments.length).to eq(0)
      end
      it 'tests success (3)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[alfa bravo charlie delta])
        expect(res.results.length).to eq(1)
        expect(res.results[0].param_id).to eq(:path)
        expect(res.results[0].results.length).to eq(4)
        expect(res.results[0].results[0].value).to eq('alfa')
        expect(res.results[0].results[1].value).to eq('bravo')
        expect(res.results[0].results[2].value).to eq('charlie')
        expect(res.results[0].results[3].value).to eq('delta')
        expect(res.remaining_arguments.length).to eq(0)
      end
      it 'tests success (4)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[alfa bravo charlie delta echo])
        expect(res.results.length).to eq(1)
        expect(res.results[0].param_id).to eq(:path)
        expect(res.results[0].results.length).to eq(4)
        expect(res.results[0].results[0].value).to eq('alfa')
        expect(res.results[0].results[1].value).to eq('bravo')
        expect(res.results[0].results[2].value).to eq('charlie')
        expect(res.results[0].results[3].value).to eq('delta')
        expect(res.remaining_arguments.length).to eq(1)
        expect(res.remaining_arguments[0]).to eq('echo')
      end
      it 'tests failure (1)' do
        expect do
          AP::Parser.new.parse_arguments(@syntax, %w[alfa bravo charlie delta echo], strict: true)
        end .to raise_error AP::ParseError
      end
    end
  end
end
