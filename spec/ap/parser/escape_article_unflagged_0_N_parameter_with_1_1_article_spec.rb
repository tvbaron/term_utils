require 'term_utils/ap'
include TermUtils

RSpec.describe AP::Parser do
  describe 'escape article unflagged 0-N parameter with 1-1 article' do
    before(:all) do
      @syntax = AP.create_syntax do |s|
        s.define_parameter :help do |p|
          p.define_flag '--help'
          p.define_flag '-h'
        end
        s.define_parameter :limit do |p|
          p.define_flag '--limit'
          p.define_flag '-l'
          p.define_article :v
        end
        s.define_parameter :info do |p|
          p.max_occurs = nil
          p.define_article :i1
        end
        s.define_parameter :extra do |p|
          p.max_occurs = nil
          p.define_article :e1
        end
      end
    end
    it 'tests success (1)' do
      res = AP::Parser.new.parse_arguments(@syntax, %w[alfa bravo charlie -l 3 delta echo foxtrot -h golf hotel india])
      expect(res.results.length).to eq(11)
      expect(res.results[0].param_id).to eq(:info)
      expect(res.results[0].results.length).to eq(1)
      expect(res.results[0].results[0].art_id).to eq(:i1)
      expect(res.results[0].results[0].value).to eq('alfa')
      expect(res.results[1].param_id).to eq(:info)
      expect(res.results[1].results.length).to eq(1)
      expect(res.results[1].results[0].art_id).to eq(:i1)
      expect(res.results[1].results[0].value).to eq('bravo')
      expect(res.results[2].param_id).to eq(:info)
      expect(res.results[2].results.length).to eq(1)
      expect(res.results[2].results[0].art_id).to eq(:i1)
      expect(res.results[2].results[0].value).to eq('charlie')
      expect(res.results[3].param_id).to eq(:limit)
      expect(res.results[3].results.length).to eq(1)
      expect(res.results[3].results[0].value).to eq('3')
      expect(res.results[4].param_id).to eq(:info)
      expect(res.results[4].results.length).to eq(1)
      expect(res.results[4].results[0].art_id).to eq(:i1)
      expect(res.results[4].results[0].value).to eq('delta')
      expect(res.results[5].param_id).to eq(:info)
      expect(res.results[5].results.length).to eq(1)
      expect(res.results[5].results[0].art_id).to eq(:i1)
      expect(res.results[5].results[0].value).to eq('echo')
      expect(res.results[6].param_id).to eq(:info)
      expect(res.results[6].results.length).to eq(1)
      expect(res.results[6].results[0].art_id).to eq(:i1)
      expect(res.results[6].results[0].value).to eq('foxtrot')
      expect(res.results[7].param_id).to eq(:help)
      expect(res.results[7].results.length).to eq(0)
      expect(res.results[8].param_id).to eq(:info)
      expect(res.results[8].results.length).to eq(1)
      expect(res.results[8].results[0].art_id).to eq(:i1)
      expect(res.results[8].results[0].value).to eq('golf')
      expect(res.results[9].param_id).to eq(:info)
      expect(res.results[9].results.length).to eq(1)
      expect(res.results[9].results[0].art_id).to eq(:i1)
      expect(res.results[9].results[0].value).to eq('hotel')
      expect(res.results[10].param_id).to eq(:info)
      expect(res.results[10].results.length).to eq(1)
      expect(res.results[10].results[0].art_id).to eq(:i1)
      expect(res.results[10].results[0].value).to eq('india')
    end
    it 'tests failure (1)' do
      expect do
        AP::Parser.new.parse_arguments(@syntax, %w[- alfa bravo charlie -l - 3 delta echo foxtrot -h golf hotel india])
      end .to raise_error AP::ParseError
    end
  end
end
