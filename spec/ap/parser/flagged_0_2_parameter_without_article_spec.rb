require 'term_utils/ap'
include TermUtils
RSpec.describe AP::Parser do
  describe "flagged 0-2 parameter without article" do
    before(:all) do
      @syntax = AP.create_syntax do |s|
        s.define_parameter :verbose do |p|
          p.define_flag '--verbose'
          p.define_flag '-v'
          p.max_occurs = 2
        end
      end
    end
    it "tests success (0)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[], strict: true)
      verbose_param = res.find_parameter(:verbose)
      expect(verbose_param).to eq(nil)
    end
    it "tests success (1)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[--verbose], strict: true)
      verbose_params = res.find_parameters(:verbose)
      expect(verbose_params.length).to eq(1)
    end
    it "tests success (2)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[-v], strict: true)
      verbose_params = res.find_parameters(:verbose)
      expect(verbose_params.length).to eq(1)
    end
    it "tests success (3)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[--verbose --verbose], strict: true)
      verbose_params = res.find_parameters(:verbose)
      expect(verbose_params.length).to eq(2)
    end
    it "tests success (4)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[-v -v], strict: true)
      verbose_params = res.find_parameters(:verbose)
      expect(verbose_params.length).to eq(2)
    end
    it "tests failure (0)" do
      expect do
        AP::Parser.new.parse_arguments(@syntax, %w[--verbose --verbose --verbose], strict: true)
      end .to raise_error AP::ParseError
    end
    it "tests failure (1)" do
      expect do
        AP::Parser.new.parse_arguments(@syntax, %w[-v -v -v], strict: true)
      end .to raise_error AP::ParseError
    end
  end
end
