require 'term_utils/ap'
include TermUtils

RSpec.describe AP::Parser do
  describe 'escape article unflagged 0-1 parameter with 1-1 article' do
    before(:all) do
      @syntax = AP.create_syntax do |s|
        s.define_parameter :help do |p|
          p.define_flag '--help'
          p.define_flag '-h'
        end
        s.define_parameter :limit do |p|
          p.define_flag '--limit'
          p.define_flag '-l'
          p.define_article :v
        end
        s.define_parameter :info do |p|
          p.define_article :i1
        end
        s.define_parameter :extra do |p|
          p.define_article :e1
        end
      end
    end
    it 'tests success (1)' do
      res = AP::Parser.new.parse_arguments(@syntax, %w[alfa bravo -l 3 charlie delta -h echo foxtrot])
      expect(res.results.length).to eq(3)
      expect(res.results[0].param_id).to eq(:info)
      expect(res.results[0].results.length).to eq(1)
      expect(res.results[0].results[0].art_id).to eq(:i1)
      expect(res.results[0].results[0].value).to eq('alfa')
      expect(res.results[1].param_id).to eq(:extra)
      expect(res.results[1].results.length).to eq(1)
      expect(res.results[1].results[0].art_id).to eq(:e1)
      expect(res.results[1].results[0].value).to eq('bravo')
      expect(res.results[2].param_id).to eq(:limit)
      expect(res.results[2].results.length).to eq(1)
      expect(res.results[2].results[0].value).to eq('3')
      expect(res.remaining_arguments.length).to eq(5)
      expect(res.remaining_arguments[0]).to eq('charlie')
      expect(res.remaining_arguments[1]).to eq('delta')
      expect(res.remaining_arguments[2]).to eq('-h')
      expect(res.remaining_arguments[3]).to eq('echo')
      expect(res.remaining_arguments[4]).to eq('foxtrot')
    end
    it 'tests failure (1)' do
      expect do
        AP::Parser.new.parse_arguments(@syntax, %w[- alfa bravo -l 3 charlie delta -h echo foxtrot])
      end .to raise_error AP::ParseError
    end
    it 'tests failure (2)' do
      expect do
        AP::Parser.new.parse_arguments(@syntax, %w[alfa - bravo -l 3 charlie delta -h echo foxtrot])
      end .to raise_error AP::ParseError
    end
    it 'tests success (2)' do
      res = AP::Parser.new.parse_arguments(@syntax, %w[alfa bravo - -l 3 charlie delta -h echo foxtrot])
      expect(res.results.length).to eq(2)
      expect(res.results[0].param_id).to eq(:info)
      expect(res.results[0].results.length).to eq(1)
      expect(res.results[0].results[0].art_id).to eq(:i1)
      expect(res.results[0].results[0].value).to eq('alfa')
      expect(res.results[1].param_id).to eq(:extra)
      expect(res.results[1].results.length).to eq(1)
      expect(res.results[1].results[0].art_id).to eq(:e1)
      expect(res.results[1].results[0].value).to eq('bravo')
      expect(res.remaining_arguments.length).to eq(8)
      expect(res.remaining_arguments[0]).to eq('-')
      expect(res.remaining_arguments[1]).to eq('-l')
      expect(res.remaining_arguments[2]).to eq('3')
      expect(res.remaining_arguments[3]).to eq('charlie')
      expect(res.remaining_arguments[4]).to eq('delta')
      expect(res.remaining_arguments[5]).to eq('-h')
      expect(res.remaining_arguments[6]).to eq('echo')
      expect(res.remaining_arguments[7]).to eq('foxtrot')
    end
    it 'tests success (3)' do
      res = AP::Parser.new.parse_arguments(@syntax, %w[alfa bravo -l 3 - charlie delta -h echo foxtrot])
      expect(res.results.length).to eq(3)
      expect(res.results[0].param_id).to eq(:info)
      expect(res.results[0].results.length).to eq(1)
      expect(res.results[0].results[0].art_id).to eq(:i1)
      expect(res.results[0].results[0].value).to eq('alfa')
      expect(res.results[1].param_id).to eq(:extra)
      expect(res.results[1].results.length).to eq(1)
      expect(res.results[1].results[0].art_id).to eq(:e1)
      expect(res.results[1].results[0].value).to eq('bravo')
      expect(res.results[2].param_id).to eq(:limit)
      expect(res.results[2].results.length).to eq(1)
      expect(res.results[2].results[0].value).to eq('3')
      expect(res.remaining_arguments.length).to eq(6)
      expect(res.remaining_arguments[0]).to eq('-')
      expect(res.remaining_arguments[1]).to eq('charlie')
      expect(res.remaining_arguments[2]).to eq('delta')
      expect(res.remaining_arguments[3]).to eq('-h')
      expect(res.remaining_arguments[4]).to eq('echo')
      expect(res.remaining_arguments[5]).to eq('foxtrot')
    end
  end
end
