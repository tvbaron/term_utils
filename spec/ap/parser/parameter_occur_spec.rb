require 'term_utils/ap'
include TermUtils

RSpec.describe AP::Parser do
  describe 'parameter occur' do
    describe 'flagged parameter without article' do
      before(:all) do
        @syntax = AP.create_syntax do |s|
          s.define_parameter :verbose do |p|
            p.min_occurs = 1
            p.max_occurs = 2
            p.define_flag '-v'
          end
          s.define_parameter :default do |p|
            p.define_flag '-d'
          end
        end
      end
      it 'tests failure (1)' do
        expect do
          AP::Parser.new.parse_arguments(@syntax, %w[], strict: true)
        end .to raise_error AP::ParseError
      end
      it 'tests success (1)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[-v], strict: true)
        expect(res.results.length).to eq(1)
        expect(res.results[0].param_id).to eq(:verbose)
        expect(res.results[0].results.length).to eq(0)
        expect(res.remaining_arguments.length).to eq(0)
      end
      it 'tests success (2)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[-v -v], strict: true)
        expect(res.results.length).to eq(2)
        expect(res.results[0].param_id).to eq(:verbose)
        expect(res.results[0].results.length).to eq(0)
        expect(res.results[1].param_id).to eq(:verbose)
        expect(res.results[1].results.length).to eq(0)
        expect(res.remaining_arguments.length).to eq(0)
      end
      it 'tests success (3)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[-v -v -v])
        expect(res.results.length).to eq(2)
        expect(res.results[0].param_id).to eq(:verbose)
        expect(res.results[0].results.length).to eq(0)
        expect(res.results[1].param_id).to eq(:verbose)
        expect(res.results[1].results.length).to eq(0)
        expect(res.remaining_arguments.length).to eq(1)
        expect(res.remaining_arguments[0]).to eq('-v')
      end
      it 'tests failure (2)' do
        expect do
          AP::Parser.new.parse_arguments(@syntax, %w[-v -v -v], strict: true)
        end .to raise_error AP::ParseError
      end
      it 'tests success (4)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[-v -d -d])
        expect(res.results.length).to eq(2)
        expect(res.results[0].param_id).to eq(:verbose)
        expect(res.results[0].results.length).to eq(0)
        expect(res.results[1].param_id).to eq(:default)
        expect(res.results[1].results.length).to eq(0)
        expect(res.remaining_arguments.length).to eq(1)
        expect(res.remaining_arguments[0]).to eq('-d')
      end
      it 'tests failure (3)' do
        expect do
          AP::Parser.new.parse_arguments(@syntax, %w[-v -d -d], strict: true)
        end .to raise_error AP::ParseError
      end
    end
    describe 'flagged parameter with article' do
      before(:all) do
        @syntax = AP.create_syntax do |s|
          s.define_parameter :limit do |p|
            p.min_occurs = 2
            p.max_occurs = 4
            p.define_flag '-l'
            p.define_article
          end
          s.define_parameter :mindepth do |p|
            p.define_flag '-mindepth'
            p.define_article
          end
        end
      end
      it 'tests failure (1)' do
        expect do
          AP::Parser.new.parse_arguments(@syntax, %w[], strict: true)
        end .to raise_error AP::ParseError
      end
      it 'tests failure (2)' do
        expect do
          AP::Parser.new.parse_arguments(@syntax, %w[-l 1], strict: true)
        end .to raise_error AP::ParseError
      end
      it 'tests success (1)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[-l 1 -l 2], strict: true)
        expect(res.results.length).to eq(2)
        expect(res.results[0].param_id).to eq(:limit)
        expect(res.results[0].results.length).to eq(1)
        expect(res.results[0].results[0].value).to eq('1')
        expect(res.results[1].param_id).to eq(:limit)
        expect(res.results[1].results.length).to eq(1)
        expect(res.results[1].results[0].value).to eq('2')
        expect(res.remaining_arguments.length).to eq(0)
      end
      it 'tests success (2)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[-l 1 -l 2 -l 3], strict: true)
        expect(res.results.length).to eq(3)
        expect(res.results[0].param_id).to eq(:limit)
        expect(res.results[0].results.length).to eq(1)
        expect(res.results[0].results[0].value).to eq('1')
        expect(res.results[1].param_id).to eq(:limit)
        expect(res.results[1].results.length).to eq(1)
        expect(res.results[1].results[0].value).to eq('2')
        expect(res.results[2].param_id).to eq(:limit)
        expect(res.results[2].results.length).to eq(1)
        expect(res.results[2].results[0].value).to eq('3')
        expect(res.remaining_arguments.length).to eq(0)
      end
      it 'tests success (3)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[-l 1 -l 2 -l 3 -l 4], strict: true)
        expect(res.results.length).to eq(4)
        expect(res.results[0].param_id).to eq(:limit)
        expect(res.results[0].results.length).to eq(1)
        expect(res.results[0].results[0].value).to eq('1')
        expect(res.results[1].param_id).to eq(:limit)
        expect(res.results[1].results.length).to eq(1)
        expect(res.results[1].results[0].value).to eq('2')
        expect(res.results[2].param_id).to eq(:limit)
        expect(res.results[2].results.length).to eq(1)
        expect(res.results[2].results[0].value).to eq('3')
        expect(res.results[3].param_id).to eq(:limit)
        expect(res.results[3].results.length).to eq(1)
        expect(res.results[3].results[0].value).to eq('4')
        expect(res.remaining_arguments.length).to eq(0)
      end
      it 'tests success (4)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[-l 1 -l 2 -l 3 -l 4 -l 5])
        expect(res.results.length).to eq(4)
        expect(res.results[0].param_id).to eq(:limit)
        expect(res.results[0].results.length).to eq(1)
        expect(res.results[0].results[0].value).to eq('1')
        expect(res.results[1].param_id).to eq(:limit)
        expect(res.results[1].results.length).to eq(1)
        expect(res.results[1].results[0].value).to eq('2')
        expect(res.results[2].param_id).to eq(:limit)
        expect(res.results[2].results.length).to eq(1)
        expect(res.results[2].results[0].value).to eq('3')
        expect(res.results[3].param_id).to eq(:limit)
        expect(res.results[3].results.length).to eq(1)
        expect(res.results[3].results[0].value).to eq('4')
        expect(res.remaining_arguments.length).to eq(2)
        expect(res.remaining_arguments[0]).to eq('-l')
        expect(res.remaining_arguments[1]).to eq('5')
      end
      it 'tests failure (3)' do
        expect do
          AP::Parser.new.parse_arguments(@syntax, %w[-l 1 -l 2 -l 3 -l 4 -l 5], strict: true)
        end .to raise_error AP::ParseError
      end
      it 'tests success (5)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[-l 1 -l 2 -mindepth 1 -mindepth 2])
        expect(res.results.length).to eq(3)
        expect(res.results[0].param_id).to eq(:limit)
        expect(res.results[0].results.length).to eq(1)
        expect(res.results[0].results[0].value).to eq('1')
        expect(res.results[1].param_id).to eq(:limit)
        expect(res.results[1].results.length).to eq(1)
        expect(res.results[1].results[0].value).to eq('2')
        expect(res.results[2].param_id).to eq(:mindepth)
        expect(res.results[2].results.length).to eq(1)
        expect(res.results[2].results[0].value).to eq('1')
        expect(res.remaining_arguments.length).to eq(2)
        expect(res.remaining_arguments[0]).to eq('-mindepth')
        expect(res.remaining_arguments[1]).to eq('2')
      end
      it 'tests failure (4)' do
        expect do
          AP::Parser.new.parse_arguments(@syntax, %w[-l 1 -l 2 -mindepth 1 -mindepth 2], strict: true)
        end .to raise_error AP::ParseError
      end
    end
    describe 'unflagged parameter - default' do
      before(:all) do
        @syntax = AP.create_syntax do |s|
          s.define_parameter :path do |p|
            p.define_article
          end
        end
      end
      it 'tests success (1)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[], strict: true)
        expect(res.results.length).to eq(0)
        expect(res.remaining_arguments.length).to eq(0)
      end
      it 'tests success (2)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[alfa], strict: true)
        expect(res.results.length).to eq(1)
        expect(res.results[0].param_id).to eq(:path)
        expect(res.results[0].results.length).to eq(1)
        expect(res.results[0].results[0].value).to eq('alfa')
        expect(res.remaining_arguments.length).to eq(0)
      end
      it 'tests success (3)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[alfa bravo])
        expect(res.results.length).to eq(1)
        expect(res.results[0].param_id).to eq(:path)
        expect(res.results[0].results.length).to eq(1)
        expect(res.results[0].results[0].value).to eq('alfa')
        expect(res.remaining_arguments.length).to eq(1)
        expect(res.remaining_arguments[0]).to eq('bravo')
      end
      it 'tests failure (1)' do
        expect do
          AP::Parser.new.parse_arguments(@syntax, %w[alfa bravo], strict: true)
        end .to raise_error AP::ParseError
      end
    end
    describe 'unflagged parameter' do
      before(:all) do
        @syntax = AP.create_syntax do |s|
          s.define_parameter :path do |p|
            p.min_occurs = 1
            p.max_occurs = 3
            p.define_article
          end
        end
      end
      it 'tests failure (1)' do
        expect do
          AP::Parser.new.parse_arguments(@syntax, %w[])
        end .to raise_error AP::ParseError
      end
      it 'tests success (1)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[alfa])
        expect(res.results.length).to eq(1)
        expect(res.results[0].param_id).to eq(:path)
        expect(res.results[0].results.length).to eq(1)
        expect(res.results[0].results[0].value).to eq('alfa')
        expect(res.remaining_arguments.length).to eq(0)
      end
      it 'tests success (2)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[alfa bravo])
        expect(res.results.length).to eq(2)
        expect(res.results[0].param_id).to eq(:path)
        expect(res.results[0].results.length).to eq(1)
        expect(res.results[0].results[0].value).to eq('alfa')
        expect(res.results[1].param_id).to eq(:path)
        expect(res.results[1].results.length).to eq(1)
        expect(res.results[1].results[0].value).to eq('bravo')
        expect(res.remaining_arguments.length).to eq(0)
      end
      it 'tests success (3)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[alfa bravo charlie])
        expect(res.results.length).to eq(3)
        expect(res.results[0].param_id).to eq(:path)
        expect(res.results[0].results.length).to eq(1)
        expect(res.results[0].results[0].value).to eq('alfa')
        expect(res.results[1].param_id).to eq(:path)
        expect(res.results[1].results.length).to eq(1)
        expect(res.results[1].results[0].value).to eq('bravo')
        expect(res.results[2].param_id).to eq(:path)
        expect(res.results[2].results.length).to eq(1)
        expect(res.results[2].results[0].value).to eq('charlie')
        expect(res.remaining_arguments.length).to eq(0)
      end
      it 'tests success (4)' do
        res = AP::Parser.new.parse_arguments(@syntax, %w[alfa bravo charlie delta])
        expect(res.results.length).to eq(3)
        expect(res.results[0].param_id).to eq(:path)
        expect(res.results[0].results.length).to eq(1)
        expect(res.results[0].results[0].value).to eq('alfa')
        expect(res.results[1].param_id).to eq(:path)
        expect(res.results[1].results.length).to eq(1)
        expect(res.results[1].results[0].value).to eq('bravo')
        expect(res.results[2].param_id).to eq(:path)
        expect(res.results[2].results.length).to eq(1)
        expect(res.results[2].results[0].value).to eq('charlie')
        expect(res.remaining_arguments.length).to eq(1)
        expect(res.remaining_arguments[0]).to eq('delta')
      end
      it 'tests failure (1)' do
        expect do
          AP::Parser.new.parse_arguments(@syntax, %w[alfa bravo charlie delta], strict: true)
        end .to raise_error AP::ParseError
      end
    end
  end
end
