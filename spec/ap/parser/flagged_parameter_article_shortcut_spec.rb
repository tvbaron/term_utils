require 'term_utils/ap'
include TermUtils
RSpec.describe AP::Parser do
  describe "flagged parameter article shortcut" do
    before(:all) do
      @syntax = AP.create_syntax do |s|
        s.define_parameter :define do |p|
          p.define_flag '--define'
          p.define_flag '-D'
          p.max_occurs = nil
          p.define_article
        end
      end
    end
    it "tests success (1)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[--define alfa])
      expect(res.results.length).to eq(1)
      expect(res.results[0].param_id).to eq(:define)
      expect(res.results[0].results.length).to eq(1)
      expect(res.results[0].results[0].value).to eq('alfa')
      expect(res.remaining_arguments.length).to eq(0)
    end
    it "tests success (2)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[--define=bravo])
      expect(res.results.length).to eq(1)
      expect(res.results[0].param_id).to eq(:define)
      expect(res.results[0].results.length).to eq(1)
      expect(res.results[0].results[0].value).to eq('bravo')
      expect(res.remaining_arguments.length).to eq(0)
    end
    it "tests success (3)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[-D charlie])
      expect(res.results.length).to eq(1)
      expect(res.results[0].param_id).to eq(:define)
      expect(res.results[0].results.length).to eq(1)
      expect(res.results[0].results[0].value).to eq('charlie')
      expect(res.remaining_arguments.length).to eq(0)
    end
    it "tests success (4)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[-Ddelta])
      expect(res.results.length).to eq(1)
      expect(res.results[0].param_id).to eq(:define)
      expect(res.results[0].results.length).to eq(1)
      expect(res.results[0].results[0].value).to eq('delta')
      expect(res.remaining_arguments.length).to eq(0)
    end
  end
end
