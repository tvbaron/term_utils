require 'term_utils/ap'
include TermUtils
RSpec.describe AP::Parser do
  describe "unflagged 0-1 parameter with one 1-1 article and one 1-N article" do
    before(:all) do
      @syntax = AP.create_syntax do |s|
        s.define_parameter :pair do |p|
          p.define_article :key
          p.define_article :value, max_occurs: nil
        end
      end
    end
    it "tests success (0)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[])
      value_param = res.find_parameter(:pair)
      expect(value_param).to eq(nil)
    end
    it "tests failure (0)" do
      expect do
        AP::Parser.new.parse_arguments(@syntax, %w[alfa])
      end .to raise_error AP::ParseError
    end
    it "tests success (1)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[alfa bravo])
      pair_params = res.find_parameters(:pair)
      expect(pair_params.length).to eq(1)
      pair_param = pair_params[0]
      key_arts = pair_param.find_articles(:key)
      expect(key_arts.length).to eq(1)
      key_art = key_arts[0]
      expect(key_art.value).to eq('alfa')
      value_arts = pair_param.find_articles(:value)
      expect(value_arts.length).to eq(1)
      value_art = value_arts[0]
      expect(value_art.value).to eq('bravo')
    end
    it "tests success (2)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[alfa bravo charlie])
      pair_params = res.find_parameters(:pair)
      expect(pair_params.length).to eq(1)
      pair_param = pair_params[0]
      key_arts = pair_param.find_articles(:key)
      expect(key_arts.length).to eq(1)
      key_art = key_arts[0]
      expect(key_art.value).to eq('alfa')
      value_arts = pair_param.find_articles(:value)
      expect(value_arts.length).to eq(2)
      value_art = value_arts[0]
      expect(value_art.value).to eq('bravo')
      value_art = value_arts[1]
      expect(value_art.value).to eq('charlie')
    end
  end
end
