require 'term_utils/ap'
include TermUtils

RSpec.describe AP::Parser do
  describe 'error' do
    describe 'flagged parameter unexpected' do
      before(:all) do
        @syntax = AP.create_syntax
      end
      it 'tests failure' do
        err = nil
        begin
          AP::Parser.new.parse_arguments(@syntax, %w[-l], strict: true)
        rescue AP::ParseError => e
          expect(e.message).to eq('flagged parameter unexpected (fault: "-l")')
          expect(e.short_message).to eq('flagged parameter unexpected')
          expect(e.parameter?).to eq(false)
          expect(e.parameter).to eq(nil)
          expect(e.fault?).to eq(true)
          expect(e.fault).to eq('-l')
          err = e
        end
        expect(err).to_not be_nil
      end
    end
    describe 'unflagged parameter unexpected' do
      before(:all) do
        @syntax = AP.create_syntax
      end
      it 'tests failure' do
        err = nil
        begin
          AP::Parser.new.parse_arguments(@syntax, %w[foo], strict: true)
        rescue AP::ParseError => e
          expect(e.message).to eq('unflagged parameter unexpected (fault: "foo")')
          expect(e.short_message).to eq('unflagged parameter unexpected')
          expect(e.parameter?).to eq(false)
          expect(e.parameter).to eq(nil)
          expect(e.fault?).to eq(true)
          expect(e.fault).to eq('foo')
          err = e
        end
        expect(err).to_not be_nil
      end
    end
    describe 'occur limit reached' do
      before(:all) do
        @syntax = AP.create_syntax do |s|
          s.define_parameter :verbose do |p|
            p.define_flag '-v'
          end
        end
      end
      it 'tests failure' do
        err = nil
        begin
          AP::Parser.new.parse_arguments(@syntax, %w[-v -v], strict: true)
        rescue AP::ParseError => e
          expect(e.message).to eq('occur limit reached (parameter: "verbose", fault: "-v")')
          expect(e.short_message).to eq('occur limit reached')
          expect(e.parameter?).to eq(true)
          expect(e.parameter).to eq(:verbose)
          expect(e.fault?).to eq(true)
          expect(e.fault).to eq('-v')
          err = e
        end
        expect(err).to_not be_nil
      end
    end
    describe 'parameter not consumed' do
      before(:all) do
        @syntax = AP.create_syntax do |s|
          s.define_parameter :extra do |p|
            p.min_occurs = 1
            p.define_article
          end
        end
      end
      it 'tests failure (1)' do
        err = nil
        begin
          AP::Parser.new.parse_arguments(@syntax, %w[], strict: true)
        rescue AP::ParseError => e
          expect(e.message).to eq('parameter not consumed (parameter: "extra")')
          expect(e.short_message).to eq('parameter not consumed')
          expect(e.parameter?).to eq(true)
          expect(e.parameter).to eq(:extra)
          expect(e.fault?).to eq(false)
          expect(e.fault).to eq(nil)
          err = e
        end
        expect(err).to_not be_nil
      end
      it 'tests failure (2)' do
        err = nil
        begin
          AP::Parser.new.parse_arguments(@syntax, %w[--], strict: true)
        rescue AP::ParseError => e
          expect(e.message).to eq('parameter not consumed (parameter: "extra")')
          expect(e.short_message).to eq('parameter not consumed')
          expect(e.parameter?).to eq(true)
          expect(e.parameter).to eq(:extra)
          expect(e.fault?).to eq(false)
          expect(e.fault).to eq(nil)
          err = e
        end
        expect(err).to_not be_nil
      end
    end
    describe 'article not consumed' do
      before(:all) do
        @syntax = AP.create_syntax do |s|
          s.define_parameter :limit do |p|
            p.define_flag '-l'
            p.define_article
          end
          s.define_parameter :value do |p|
            p.define_article
            p.define_article
          end
        end
      end
      it 'tests failure (1)' do
        err = nil
        begin
          AP::Parser.new.parse_arguments(@syntax, %w[-l])
        rescue AP::ParseError => e
          expect(e.message).to eq('article not consumed (parameter: "limit")')
          expect(e.short_message).to eq('article not consumed')
          expect(e.parameter?).to eq(true)
          expect(e.parameter).to eq(:limit)
          expect(e.fault?).to eq(false)
          expect(e.fault).to eq(nil)
          err = e
        end
        expect(err).to_not be_nil
      end
      it 'tests failure (2)' do
        err = nil
        begin
          AP::Parser.new.parse_arguments(@syntax, %w[foo])
        rescue AP::ParseError => e
          expect(e.message).to eq('article not consumed (parameter: "value")')
          expect(e.short_message).to eq('article not consumed')
          expect(e.parameter?).to eq(true)
          expect(e.parameter).to eq(:value)
          expect(e.fault?).to eq(false)
          expect(e.fault).to eq(nil)
          err = e
        end
        expect(err).to_not be_nil
      end
    end
  end
end
