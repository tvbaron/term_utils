require 'term_utils/ap'
include TermUtils
RSpec.describe AP::Parser do
  describe "flagged 0-1 parameter without article" do
    before(:all) do
      @syntax = AP.create_syntax do |s|
        s.define_parameter :debug do |p|
          p.define_flag '--debug'
          p.define_flag '-d'
        end
      end
    end
    it "tests success (0)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[])
      debug_param = res.find_parameter(:debug)
      expect(debug_param).to eq(nil)
    end
    it "tests success (1)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[--debug])
      debug_params = res.find_parameters(:debug)
      expect(debug_params.length).to eq(1)
    end
    it "tests success (2)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[-d])
      debug_params = res.find_parameters(:debug)
      expect(debug_params.length).to eq(1)
    end
    it "tests failure (0)" do
      expect do
        AP::Parser.new.parse_arguments(@syntax, %w[--debug --debug], strict: true)
      end .to raise_error AP::ParseError
    end
    it "tests failure (1)" do
      expect do
        AP::Parser.new.parse_arguments(@syntax, %w[-d -d], strict: true)
      end .to raise_error AP::ParseError
    end
  end
end
