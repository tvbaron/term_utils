require 'term_utils/ap'
include TermUtils
RSpec.describe AP::Parser do
  describe "unflagged 0-1 parameter with two 1-1 articles" do
    before(:all) do
      @syntax = AP.create_syntax do |s|
        s.define_parameter :pair do |p|
          p.define_article :key
          p.define_article :value
        end
      end
    end
    it "tests success (0)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[])
      value_param = res.find_parameter(:pair)
      expect(value_param).to eq(nil)
    end
    it "tests failure (0)" do
      expect do
        AP::Parser.new.parse_arguments(@syntax, %w[alfa])
      end .to raise_error AP::ParseError
    end
    it "tests success (1)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[alfa bravo])
      pair_params = res.find_parameters(:pair)
      expect(pair_params.length).to eq(1)
      pair_param = pair_params[0]
      key_arts = pair_param.find_articles(:key)
      expect(key_arts.length).to eq(1)
      key_art = key_arts[0]
      expect(key_art.value).to eq('alfa')
      value_arts = pair_param.find_articles(:value)
      expect(value_arts.length).to eq(1)
      value_art = value_arts[0]
      expect(value_art.value).to eq('bravo')
    end
    it "tests failure (1)" do
      expect do
        AP::Parser.new.parse_arguments(@syntax, %w[alfa bravo charlie], strict: true)
      end .to raise_error AP::ParseError
    end
    it "tests failure (2)" do
      expect do
        AP::Parser.new.parse_arguments(@syntax, %w[alfa bravo charlie delta], strict: true)
      end .to raise_error AP::ParseError
    end
  end
end
