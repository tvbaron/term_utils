require 'term_utils/ap'
include TermUtils
RSpec.describe AP::Parser do
  describe "flagged 0-1 parameter with one 1-1 article" do
    before(:all) do
      @syntax = AP.create_syntax do |s|
        s.define_parameter :value do |p|
          p.define_flag '--value'
          p.define_flag '-v'
          p.define_article :val
        end
      end
    end
    it "tests success (0)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[])
      value_params = res.find_parameters(:value)
      expect(value_params.length).to eq(0)
    end
    it "tests success (1)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[--value alfa])
      value_params = res.find_parameters(:value)
      expect(value_params.length).to eq(1)
      value_param = value_params.first
      expect(value_param.value).to eq('alfa')
      expect(value_param.values).to eq(%w[alfa])
      val_arts = value_param.find_articles(:val)
      expect(val_arts.length).to eq(1)
      val_art = val_arts.first
      expect(val_art.value).to eq('alfa')
    end
    it "tests success (2)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[-v alfa])
      value_params = res.find_parameters(:value)
      expect(value_params.length).to eq(1)
      value_param = value_params.first
      expect(value_param.value).to eq('alfa')
      expect(value_param.values).to eq(%w[alfa])
      val_arts = value_param.find_articles(:val)
      expect(val_arts.length).to eq(1)
      val_art = val_arts.first
      expect(val_art.value).to eq('alfa')
    end
    it "tests success (3)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[--value alfa bravo])
      expect(res.remaining_arguments).to eq(%w[bravo])
    end
    it "tests failure (4)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[-v alfa bravo])
      expect(res.remaining_arguments).to eq(%w[bravo])
    end
    it "tests failure (0)" do
      expect do
        AP::Parser.new.parse_arguments(@syntax, %w[--value alfa bravo], strict: true)
      end .to raise_error AP::ParseError
    end
    it "tests failure (1)" do
      expect do
        AP::Parser.new.parse_arguments(@syntax, %w[-v alfa bravo], strict: true)
      end .to raise_error AP::ParseError
    end
    it "tests failure (2)" do
      expect do
        AP::Parser.new.parse_arguments(@syntax, %w[--value alfa --value bravo], strict: true)
      end .to raise_error AP::ParseError
    end
    it "tests failure (3)" do
      expect do
        AP::Parser.new.parse_arguments(@syntax, %w[-v alfa -v bravo], strict: true)
      end .to raise_error AP::ParseError
    end
  end
end
