require 'term_utils/ap'
include TermUtils
RSpec.describe AP::Parser do
  describe "unflagged 0-1 parameter with one 1-1 article" do
    before(:all) do
      @syntax = AP.create_syntax do |s|
        s.define_parameter :value do |p|
          p.define_article :val
        end
      end
    end
    it "tests success (0)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[])
      value_param = res.find_parameter(:pair)
      expect(value_param).to eq(nil)
    end
    it "tests success (1)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[alfa])
      value_params = res.find_parameters(:value)
      expect(value_params.length).to eq(1)
      value_param = value_params[0]
      val_arts = value_param.find_articles(:val)
      expect(val_arts.length).to eq(1)
      val_art = val_arts[0]
      expect(val_art.value).to eq('alfa')
    end
    it "tests failure (0)" do
      expect do
        AP::Parser.new.parse_arguments(@syntax, %w[alfa bravo], strict: true)
      end .to raise_error AP::ParseError
    end
  end
end
