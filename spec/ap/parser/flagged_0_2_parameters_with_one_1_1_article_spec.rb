require 'term_utils/ap'
include TermUtils
RSpec.describe AP::Parser do
  describe "flagged 0-2 parameters with one 1-1 article" do
    before(:all) do
      @syntax = AP.create_syntax do |s|
        s.define_parameter :value do |p|
          p.define_flag '--value'
          p.define_flag '-v'
          p.max_occurs = 2
          p.define_article :val
        end
      end
    end
    it "tests success (0)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[])
      value_param = res.find_parameter(:value)
      expect(value_param).to eq(nil)
    end
    it "tests success (1)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[--value alfa])
      value_params = res.find_parameters(:value)
      expect(value_params.length).to eq(1)
      value_param = value_params.first
      expect(value_param.value).to eq('alfa')
      expect(value_param.values).to eq(%w[alfa])
      val_arts = value_param.find_articles(:val)
      expect(val_arts.length).to eq(1)
      val_art = val_arts.first
      expect(val_art.value).to eq('alfa')
    end
    it "tests success (2)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[-v alfa])
      value_params = res.find_parameters(:value)
      expect(value_params.length).to eq(1)
      value_param = value_params.first
      expect(value_param.value).to eq('alfa')
      expect(value_param.values).to eq(%w[alfa])
      val_arts = value_param.find_articles(:val)
      expect(val_arts.length).to eq(1)
      val_art = val_arts.first
      expect(val_art.value).to eq('alfa')
    end
    it "tests failure (0)" do
      expect do
        AP::Parser.new.parse_arguments(@syntax, %w[--value alfa bravo], strict: true)
      end .to raise_error AP::ParseError
    end
    it "tests failure (1)" do
      expect do
        AP::Parser.new.parse_arguments(@syntax, %w[-v alfa bravo], strict: true)
      end .to raise_error AP::ParseError
    end
    it "tests success (3)" do
      res = AP::Parser.new.parse_arguments(@syntax, %w[--value alfa --value bravo], strict: true)
      value_params = res.find_parameters(:value)
      expect(value_params.length).to eq(2)
      value_param = value_params[0]
      expect(value_param.value).to eq('alfa')
      expect(value_param.values).to eq(%w[alfa])
      val_arts = value_param.find_articles(:val)
      expect(val_arts.length).to eq(1)
      val_art = val_arts.first
      expect(val_art.value).to eq('alfa')
      value_param = value_params[1]
      expect(value_param.value).to eq('bravo')
      expect(value_param.values).to eq(%w[bravo])
      val_arts = value_param.find_articles(:val)
      expect(val_arts.length).to eq(1)
      val_art = val_arts.first
      expect(val_art.value).to eq('bravo')
    end
    it "tests failure (2)" do
      expect do
        AP::Parser.new.parse_arguments(@syntax, %w[--value alfa --value bravo --value charlie], strict: true)
      end .to raise_error AP::ParseError
    end
    it "tests failure (3)" do
      expect do
        AP::Parser.new.parse_arguments(@syntax, %w[-v alfa -v bravo -v charlie], strict: true)
      end .to raise_error AP::ParseError
    end
  end
end
