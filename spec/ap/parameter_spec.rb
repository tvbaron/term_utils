require 'term_utils/ap'
include TermUtils

RSpec.describe AP::Parameter do
  describe 'default' do
    it 'tests success' do
      syntax = AP.create_syntax do |s|
        s.define_parameter do |p|
          p.define_flag '--alfa'
        end
        s.define_parameter do |p|
          p.define_article
        end
      end
      expect(syntax.parameters.length).to eq(2)
      expect(syntax.parameters[0].id).to eq(nil)
      expect(syntax.parameters[0].min_occurs).to eq(0)
      expect(syntax.parameters[0].max_occurs).to eq(1)
      expect(syntax.parameters[0].flags.length).to eq(1)
      expect(syntax.parameters[0].flags[0].to_s).to eq('--alfa')
      expect(syntax.parameters[0].articles.length).to eq(0)
      expect(syntax.parameters[1].id).to eq(nil)
      expect(syntax.parameters[1].flags.length).to eq(0)
      expect(syntax.parameters[1].articles.length).to eq(1)
      expect(syntax.parameters[1].articles[0].id).to eq(nil)
      expect(syntax.parameters[1].articles[0].min_occurs).to eq(1)
      expect(syntax.parameters[1].articles[0].max_occurs).to eq(1)
      expect(syntax.parameters[1].articles[0].type).to eq(:string)
    end
  end
  describe 'empty' do
    it 'tests failure' do
      expect do
        syntax = AP.create_syntax do |s|
          s.define_parameter :alfa
        end
        syntax.finalize!
      end .to raise_error AP::SyntaxError
    end
  end
  describe 'min occurs less than 0' do
    it 'tests failure' do
      expect do
        syntax = AP.create_syntax do |s|
          s.define_parameter do |p|
            p.min_occurs = -1
            p.define_article
          end
        end
        syntax.finalize!
      end .to raise_error AP::SyntaxError
    end
  end
  describe 'min occurs equal to 0' do
    it 'tests failure' do
      syntax = AP.create_syntax do |s|
        s.define_parameter :alfa, min_occurs: 0 do |p|
          p.define_flag '--alfa'
        end
        s.define_parameter :bravo do |p|
          p.min_occurs = 0
          p.define_article :val
        end
      end
      syntax.finalize!
      expect(syntax.parameters.length).to eq(2)
      expect(syntax.parameters[0].id).to eq(:alfa)
      expect(syntax.parameters[0].min_occurs).to eq(0)
      expect(syntax.parameters[0].max_occurs).to eq(1)
      expect(syntax.parameters[0].flags.length).to eq(1)
      expect(syntax.parameters[0].flags[0].to_s).to eq('--alfa')
      expect(syntax.parameters[0].articles.length).to eq(0)
      expect(syntax.parameters[1].id).to eq(:bravo)
      expect(syntax.parameters[1].flags.length).to eq(0)
      expect(syntax.parameters[1].articles.length).to eq(1)
      expect(syntax.parameters[1].articles[0].id).to eq(:val)
      expect(syntax.parameters[1].articles[0].min_occurs).to eq(1)
      expect(syntax.parameters[1].articles[0].max_occurs).to eq(1)
      expect(syntax.parameters[1].articles[0].type).to eq(:string)
    end
  end
  describe 'max occurs less than 0' do
    it 'tests failure' do
      expect do
        syntax = AP.create_syntax do |s|
          s.define_parameter do |p|
            p.max_occurs = -1
            p.define_article
          end
        end
        syntax.finalize!
      end .to raise_error AP::SyntaxError
    end
  end
  describe 'max occurs equal to 0' do
    it 'tests success' do
      syntax = AP.create_syntax do |s|
        s.define_parameter :alfa do |p|
          p.max_occurs = 0
          p.define_article
        end
      end
      syntax.finalize!
      expect(syntax.parameters.length).to eq(1)
      expect(syntax.parameters[0].id).to eq(:alfa)
      expect(syntax.parameters[0].min_occurs).to eq(0)
      expect(syntax.parameters[0].max_occurs).to eq(0)
      expect(syntax.parameters[0].flags.length).to eq(0)
      expect(syntax.parameters[0].articles.length).to eq(1)
    end
  end
  describe 'max occurs less than min occurs' do
    it 'tests failure' do
      expect do
        syntax = AP.create_syntax do |s|
          s.define_parameter do |p|
            p.min_occurs = 2
            p.max_occurs = 1
            p.define_article
          end
        end
        syntax.finalize!
      end .to raise_error AP::SyntaxError
    end
  end
  describe 'max occurs equal to min occurs' do
    it 'tests success' do
      syntax = AP.create_syntax do |s|
        s.define_parameter :alfa do |p|
          p.min_occurs = 3
          p.max_occurs = 3
          p.define_article
        end
      end
      syntax.finalize!
      expect(syntax.parameters.length).to eq(1)
      expect(syntax.parameters[0].id).to eq(:alfa)
      expect(syntax.parameters[0].min_occurs).to eq(3)
      expect(syntax.parameters[0].max_occurs).to eq(3)
      expect(syntax.parameters[0].flags.length).to eq(0)
      expect(syntax.parameters[0].articles.length).to eq(1)
    end
  end
  describe 'id may occur once' do
    it 'tests success' do
      syntax = AP.create_syntax
      alfa1_param = syntax.define_parameter :alfa
      alfa2_param = syntax.define_parameter :alfa
      expect(alfa1_param).to eq(alfa2_param)
    end
  end
  describe 'flags and articles' do
    it 'tests success' do
      syntax = AP.create_syntax do |s|
        s.define_parameter :alfa do |p|
          p.define_flag '--alfa'
        end
        s.define_parameter :bravo do |p|
          p.define_flag '-b'
        end
        s.define_parameter :charlie do |p|
          p.define_flag '--charlie'
          p.define_flag '-c'
        end
        s.define_parameter :delta do |p|
          p.define_article :d1
        end
        s.define_parameter :echo do |p|
          p.define_article :e1
          p.define_article :e2
        end
        s.define_parameter :foxtrot do |p|
          p.define_flag '--foxtrot'
          p.define_flag '-f'
          p.define_article :f1
          p.define_article :f2
          p.define_article :f3
        end
      end
      syntax.finalize!
      expect(syntax.parameters.length).to eq(6)
      expect(syntax.parameters[0].id).to eq(:alfa)
      expect(syntax.parameters[0].flags.length).to eq(1)
      expect(syntax.parameters[0].flags[0].flavor).to eq(:long)
      expect(syntax.parameters[0].flags[0].to_s).to eq('--alfa')
      expect(syntax.parameters[0].articles.length).to eq(0)
      expect(syntax.parameters[1].id).to eq(:bravo)
      expect(syntax.parameters[1].flags.length).to eq(1)
      expect(syntax.parameters[1].flags[0].flavor).to eq(:short)
      expect(syntax.parameters[1].flags[0].to_s).to eq('-b')
      expect(syntax.parameters[1].articles.length).to eq(0)
      expect(syntax.parameters[2].id).to eq(:charlie)
      expect(syntax.parameters[2].flags.length).to eq(2)
      expect(syntax.parameters[2].flags[0].flavor).to eq(:long)
      expect(syntax.parameters[2].flags[0].to_s).to eq('--charlie')
      expect(syntax.parameters[2].flags[1].flavor).to eq(:short)
      expect(syntax.parameters[2].flags[1].to_s).to eq('-c')
      expect(syntax.parameters[2].articles.length).to eq(0)
      expect(syntax.parameters[3].id).to eq(:delta)
      expect(syntax.parameters[3].flags.length).to eq(0)
      expect(syntax.parameters[3].articles.length).to eq(1)
      expect(syntax.parameters[3].articles[0].id).to eq(:d1)
      expect(syntax.parameters[4].id).to eq(:echo)
      expect(syntax.parameters[4].flags.length).to eq(0)
      expect(syntax.parameters[4].articles.length).to eq(2)
      expect(syntax.parameters[4].articles[0].id).to eq(:e1)
      expect(syntax.parameters[4].articles[1].id).to eq(:e2)
      expect(syntax.parameters[5].id).to eq(:foxtrot)
      expect(syntax.parameters[5].flags.length).to eq(2)
      expect(syntax.parameters[5].flags[0].flavor).to eq(:long)
      expect(syntax.parameters[5].flags[0].to_s).to eq('--foxtrot')
      expect(syntax.parameters[5].flags[1].flavor).to eq(:short)
      expect(syntax.parameters[5].flags[1].to_s).to eq('-f')
      expect(syntax.parameters[5].articles.length).to eq(3)
      expect(syntax.parameters[5].articles[0].id).to eq(:f1)
      expect(syntax.parameters[5].articles[1].id).to eq(:f2)
      expect(syntax.parameters[5].articles[2].id).to eq(:f3)
    end
  end
end
