require 'term_utils/ap'
include TermUtils

RSpec.describe AP::Flag do
  describe '-' do
    it 'tests failure' do
      expect do
        AP.create_syntax do |s|
          s.define_parameter :alfa do |p|
            p.define_flag '-'
          end
        end
      end .to raise_error AP::SyntaxError
    end
  end
  describe '--' do
    it 'tests failure' do
      expect do
        AP.create_syntax do |s|
          s.define_parameter :alfa do |p|
            p.define_flag '-'
          end
        end
      end .to raise_error AP::SyntaxError
    end
  end
  describe '---' do
    it 'tests failure' do
      expect do
        AP.create_syntax do |s|
          s.define_parameter :alfa do |p|
            p.define_flag '-'
          end
        end
      end .to raise_error AP::SyntaxError
    end
  end
  describe '-a' do
    it 'tests success' do
      syntax = AP.create_syntax do |s|
        s.define_parameter :alfa do |p|
          p.define_flag '-a'
        end
      end
      expect(syntax.parameters.length).to eq(1)
      expect(syntax.parameters[0].flags.length).to eq(1)
      expect(syntax.parameters[0].flags[0].to_s).to eq('-a')
      expect(syntax.parameters[0].flags[0].flavor).to eq(:short)
      expect(syntax.parameters[0].articles.length).to eq(0)
    end
  end
  describe '-a -a' do
    it 'tests failure (1)' do
      expect do
        AP.create_syntax do |s|
          s.define_parameter :alfa do |p|
            p.define_flag '-a'
            p.define_flag '-a'
          end
        end
      end .to raise_error AP::SyntaxError
    end
    it 'tests failure (2)' do
      expect do
        syntax = AP.create_syntax do |s|
          s.define_parameter :alfa do |p|
            p.define_flag '-a'
          end
          s.define_parameter :ant do |p|
            p.define_flag '-a'
          end
        end
        syntax.finalize!
      end .to raise_error AP::SyntaxError
    end
  end
  describe '--alfa' do
    it 'tests success' do
      syntax = AP.create_syntax do |s|
        s.define_parameter :alfa do |p|
          p.define_flag '--alfa'
        end
        s.define_parameter :mixdepth do |p|
          p.define_flag '-mindepth'
        end
      end
      expect(syntax.parameters.length).to eq(2)
      expect(syntax.parameters[0].flags.length).to eq(1)
      expect(syntax.parameters[0].flags[0].to_s).to eq('--alfa')
      expect(syntax.parameters[0].flags[0].flavor).to eq(:long)
      expect(syntax.parameters[0].articles.length).to eq(0)
      expect(syntax.parameters[1].flags.length).to eq(1)
      expect(syntax.parameters[1].flags[0].to_s).to eq('-mindepth')
      expect(syntax.parameters[1].flags[0].flavor).to eq(:long)
      expect(syntax.parameters[1].articles.length).to eq(0)
    end
  end
  describe '--alfa --alfa' do
    it 'tests failure (1)' do
      expect do
        AP.create_syntax do |s|
          s.define_parameter :alfa do |p|
            p.define_flag '--alfa'
            p.define_flag '--alfa'
          end
        end
      end .to raise_error AP::SyntaxError
    end
    it 'tests failure (2)' do
      expect do
        syntax = AP.create_syntax do |s|
          s.define_parameter :alfa do |p|
            p.define_flag '--alfa'
          end
          s.define_parameter :ant do |p|
            p.define_flag '--alfa'
          end
        end
        syntax.finalize!
      end .to raise_error AP::SyntaxError
    end
  end
  describe '--alfa -a' do
    it 'tests success' do
      syntax = AP.create_syntax do |s|
        s.define_parameter :alfa do |p|
          p.define_flag '--alfa'
          p.define_flag '-a'
        end
      end
      expect(syntax.parameters.length).to eq(1)
      expect(syntax.parameters[0].flags.length).to eq(2)
      expect(syntax.parameters[0].flags[0].to_s).to eq('--alfa')
      expect(syntax.parameters[0].flags[0].flavor).to eq(:long)
      expect(syntax.parameters[0].flags[1].to_s).to eq('-a')
      expect(syntax.parameters[0].flags[1].flavor).to eq(:short)
      expect(syntax.parameters[0].articles.length).to eq(0)
    end
  end
end
