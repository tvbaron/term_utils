require 'term_utils/ap'
include TermUtils

RSpec.describe AP::Article do
  describe 'default' do
    it 'tests success' do
      syntax = AP.create_syntax do |s|
        s.define_parameter :alfa do |p|
          p.define_article
        end
      end
      expect(syntax.parameters.length).to eq(1)
      expect(syntax.parameters[0].flags.length).to eq(0)
      expect(syntax.parameters[0].articles.length).to eq(1)
      expect(syntax.parameters[0].articles[0].id).to eq(nil)
      expect(syntax.parameters[0].articles[0].min_occurs).to eq(1)
      expect(syntax.parameters[0].articles[0].max_occurs).to eq(1)
      expect(syntax.parameters[0].articles[0].type).to eq(:string)
    end
  end
  describe 'min occurs less than 0' do
    it 'tests failure' do
      expect do
        syntax = AP.create_syntax do |s|
          s.define_parameter :alfa do |p|
            p.define_article do |a|
              a.min_occurs = -1
            end
          end
        end
        syntax.finalize!
      end .to raise_error AP::SyntaxError
    end
  end
  describe 'min occurs equal to 0' do
    it 'tests success' do
      syntax = AP.create_syntax do |s|
        s.define_parameter :alfa do |p|
          p.define_article :val do |a|
            a.min_occurs = 0
          end
        end
      end
      syntax.finalize!
      expect(syntax.parameters.length).to eq(1)
      expect(syntax.parameters[0].flags.length).to eq(0)
      expect(syntax.parameters[0].articles.length).to eq(1)
      expect(syntax.parameters[0].articles[0].id).to eq(:val)
      expect(syntax.parameters[0].articles[0].min_occurs).to eq(0)
      expect(syntax.parameters[0].articles[0].max_occurs).to eq(1)
      expect(syntax.parameters[0].articles[0].type).to eq(:string)
    end
  end
  describe 'max occurs less than 0' do
    it 'tests failure' do
      expect do
        syntax = AP.create_syntax do |s|
          s.define_parameter :alfa do |p|
            p.define_article do |a|
              a.max_occurs = -1
            end
          end
        end
        syntax.finalize!
      end .to raise_error AP::SyntaxError
    end
  end
  describe 'max occurs equal to 0' do
    it 'tests failure' do
      expect do
        syntax = AP.create_syntax do |s|
          s.define_parameter :alfa do |p|
            p.define_article do |a|
              a.max_occurs = 0
            end
          end
        end
        syntax.finalize!
      end .to raise_error AP::SyntaxError
    end
  end
  describe 'max occurs less than min occurs' do
    it 'tests failure' do
      expect do
        syntax = AP.create_syntax do |s|
          s.define_parameter :alfa do |p|
            p.define_article do |a|
              a.min_occurs = 2
              a.max_occurs = 1
            end
          end
        end
        syntax.finalize!
      end .to raise_error AP::SyntaxError
    end
  end
  describe 'id may occur once' do
    it 'tests success' do
      syntax = AP.create_syntax
      alfa_param = syntax.define_parameter :alfa
      a2_art = alfa_param.define_article :a1
      a1_art = alfa_param.define_article :a1
      expect(a1_art).to eq(a2_art)
    end
  end
end
