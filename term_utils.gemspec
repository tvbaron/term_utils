Gem::Specification.new do |s|
  s.name         = "term_utils"
  s.version      = "0.5.0"
  s.date         = "2023-08-09"
  s.summary      = "Utilities for terminal application."
  s.description  = <<-EOS
Utilities for terminal application like argument parsing, table formatting and file finding.
EOS
  s.authors      = ["Thomas Baron"]
  s.email        = "tvbaron at gmail dot com"
  s.files        = Dir["lib/**/*.rb", "doc/**/*"] + ["AUTHORS", "CHANGELOG.md", "COPYING", "Rakefile", "README.md", "term_utils.gemspec"]
  s.homepage     = "https://gitlab.com/tvbaron/term_utils"
  s.license      = "GPL-3.0-only"
  s.required_ruby_version = '>= 2.2.0'
  s.metadata["yard.run"] = "yri"
end
