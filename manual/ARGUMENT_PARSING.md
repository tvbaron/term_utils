# Argument Parsing

* [1. Getting Started](#1-getting-started)
* [2. Example](#2-example)
* [3. Syntax DSL](#3-syntax-dsl)
  * [3.1. Flagged Parameter](#31-flagged-parameter)
  * [3.2. Unflagged Parameter](#32-unflagged-parameter)
  * [3.3. Article](#33-article)
  * [3.4. Special Flags](#34-special-flags)
* [4. Parser](#4-parser)
* [5. Result](#5-result)

## 1. Getting Started

Install `term_utils` at the command prompt:

    gem install term_utils

Require `term_utils` in the source file:

    require 'term_utils'

or, just the argument parsing module:

    require 'term_utils/ap'

## 2. Example

Let be `example.rb`:

    require 'term_utils/ap'
    syntax = TermUtils::AP.create_syntax do |s|
      s.define_parameter :limit do |p|
        p.define_flag '-l'
        p.define_article
      end
      s.define_parameter :path do |p|
        p.max_occurs = nil
        p.define_article
      end
    end
    limit = nil
    paths = []
    TermUtils::AP.parse_arguments(syntax, ARGV) do |on|
      on.parameter :limit do |p|
        limit = p.value
      end
      on.parameter :path do |p|
        paths << p.value
      end
    end
    puts "limit: #{limit}"
    puts "paths:"
    paths.each { |p| puts "  #{p}" }

At command prompt:

    ruby example.rb alfa bravo -l 10 charlie

Output:

    limit: 10
    paths:
      alfa
      bravo
      charlie

## 3. Syntax DSL

A syntax must be defined in order to parse arguments.

    syntax = TermUtils::AP.create_syntax do |s|
        # syntax definition
    end

A syntax holds a list of parameters. A parameter is either flagged or unflagged. A parameter holds a list of articles.

    +--------+     0..* +--------------+     0..* +------+
    | Syntax | -------> | Parameter    | -------> | Flag |
    +--------+          +--------------+          +------+
                        | - id         |
                        | - min_occurs |     0..* +--------------+
                        | - max_occurs | -------> | Article      |
                        +--------------+          +--------------+
                                                  | - id         |
                                                  | - min_occurs |
                                                  | - max_occurs |
                                                  +--------------+

### 3.1. Flagged Parameter

A flagged parameter has at least one flag. Multiple flags can be defined for the same parameter.

A flagged parameter may hold articles.

By default, a flagged parameter may occur 1 time, i.e. `min_occurs` is `0` and `max_occurs` is `1`. This can be modified.

A flagged parameter may appear at any position in the argument list.

#### 3.1.1. Flag

A flag must match the following pattern: `^-[A-Za-z0-9_-]+$`. However a flag can not be composed only of `-`.

#### 3.1.2. Flagged parameter without article:

    syntax = TermUtils::AP.create_syntax do |s|
      s.define_parameter :help do |p|
        p.define_flag '--help'
        p.define_flag '-h'
      end
    end

#### 3.1.3. Flagged parameter with article:

    syntax = TermUtils::AP.create_syntax do |s|
      s.define_parameter :min_depth do |p|
        p.define_flag '-mindepth'
        p.define_article
      end
    end

### 3.2. Unflagged Parameter

An unflagged parameter has no flag.

An unflagged parameter must hold at least one article.

By default, an unflagged parameter may occur 1 time, i.e. `min_occurs` is `0` and `max_occurs` is `1`. This can be modified.

Unflagged parameters must appear in the argument list in the same order than the one of the syntax definition.

    syntax = TermUtils::AP.create_syntax do |s|
      s.define_parameter :path do |p|
        p.define_article
      end
      s.define_parameter :extra do |p|
        p.define_article
      end
    end

### 3.3. Article

An article defines a value, i.e. an argument.

By default, an article must occur exactly 1 time, i.e. `min_occurs` is `1` and `max_occurs` is `1`. This can be modified.

### 3.4. Special Flags

There are 2 special flags, `-` and `--`, used respectively to escape article and to escape a parameter.

## 4. Parser

    options = {}
    options[:strict] = true
    TermUtils::AP.parse_arguments(syntax, ARGV, options)

`parse_arguments` may raise:
- `TermUtils::AP::SyntaxError` when the syntax definition is wrong,
- `TermUtils::AP::ParseError` when the arguments do not match the syntax.

By default, the algorithm only tries to parse what is defined in the syntax and will let the application handle the remaining arguments.

The `strict` option tells the algorithm to raise a `ParseError` if it encounters unknown arguments.

## 5. Result

The parser returns a result.

    +--------+     0..* +-----------------+     0..* +---------------+
    | Result | -------> | ParameterResult | -------> | ArticleResult |
    +--------+          +-----------------+          +---------------+

Handle result directly:

    syntax = TermUtils::AP.create_syntax do |s|
      s.define_parameter :help do |p|
        p.define_flag '--help'
      end
      s.define_parameter :path do |p|
        p.max_occurs = nil
        p.define_article :p1
      end
      s.define_parameter :extra do |p|
        p.define_article :e1, max_occurs: nil
      end
    end
    res = TermUtils::AP.parse_arguments(syntax, ARGV)
    # Fetch the first parameter result for the :help parameter
    res.find_parameter :help
    # Fetch all parameter results of the :path parameter
    res.find_parameters :path
    # Fetch remaining arguments
    res.remaining_arguments
    # Fetch the first parameter result for the :extra parameter
    extra_p_res = res.find_parameter :extra
    # Fetch the first value of the :extra parameter
    extra_p_res.value
    # Fetch all values of the :extra parameter
    extra_p_res.values
    # Fetch all article results (of the :extra parameter result)
    extra_p_res.articles
    # Fetch all article results for the :e1 article
    extra_p_res.find_articles :e1
    # Fetch the first article result for the :e1 article
    e1_a_res = extra_p_res.find_article :e1
    # Fetch the value of :e1 article result
    e1_a_res.value

Handle result through callbacks:

    syntax = TermUtils::AP.create_syntax do |s|
      s.define_parameter :help do |p|
        p.define_flag '--help'
      end
      s.define_parameter :path do |p|
        p.max_occurs = nil
        p.define_article :p1
      end
      s.define_parameter :extra do |p|
        p.define_article :e1, max_occurs: nil
      end
    end
    TermUtils::AP.parse_arguments(syntax, ARGV) do |on|
      on.parameter :help do |p_res|
        # Called upon :help parameter encounter
      end
      on.parameter :path do |p_res|
        # Called upon :path parameter encounter
        # Fetch the value
        p_res.value
      end
      on.parameter :extra do |p_res|
        # Called upon :extra parameter encounter
        # Fetch the first value
        p_res.value
        # Fetch all values
        p_res.values
        # Fetch all article results
        extra_p_res.articles
        # Fetch all article results for the :e1 article
        extra_p_res.find_articles :e1
        # Fetch the first article result for the :e1 article
        e1_a_res = extra_p_res.find_article :e1
        # Fetch the value of :e1 article result
        e1_a_res.value
      end
      on.finished do |rem_args|
        # Called when parsing is finished with the remaining arguments
      end
    end
